require'nvim-treesitter.configs'.setup {
  ensure_installed = { "c", "cpp", "html", "java", "javascript", "lua", "python", "vim" },

  sync_install = false,
  auto_install = true,

  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  refactor = {
      highlight_definitions = { enable = true },
      smart_rename = {
          enable = true,
            keymaps = {
                smart_rename = "grr",
            },
      },
      navigation = {
          enable = true,
          keymaps = {
              goto_definition = "gnd",
              list_definitions = "gnD",
              list_definitions_toc = "gO",
              goto_next_usage = "<A-*>",
              goto_previous_usage = "<A-#>",
          },
      },
  },
}
