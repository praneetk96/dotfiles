#!/usr/bin/bash

# Brightness notification: dunst

icon_path=/usr/share/pixmaps/sun.png

# you can also use a better alternative : light 
# sudo pacman -S light - and then light -G should output the current brightness
# this implementation is using xbacklight

function get_brightness {
    #xbacklight -get
    brightnessctl info | grep % | cut -d '(' -f 2 | cut -d '%' -f 1
}

function brightness_notification {
    brightness=`get_brightness`
    printf -v br_int %.0f "$brightness"
    echo $br_int
    bar=$(seq -s "─" $(($br_int / 5)) | sed 's/[0-9]//g')
    dunstify -r 817 -u normal -i ${icon_path} "Brightness is at: "${br_int}"%" $bar
}

brightnessctl set +5%
brightness_notification
