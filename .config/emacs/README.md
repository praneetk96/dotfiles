# What is GNU Emacs
GNU Emacs is an extensible, customizable, free & open source text editor, but It's more than that, It is also an interpreter for **Emacs Lisp** *a dialect of the lisp programming language with extensions to support text editing.*

## Description
Here I'm uploading my Emacs's configuration so that I can quickly restore it on my machine, I'm following Derek Taylor's [guide](https://gitlab.com/dwt1/configuring-emacs) on setting up Emacs from scratch.
