import imp
import os
from libqtile import qtile
from libqtile.bar import Bar
from libqtile.widget.sep import Sep
from libqtile.widget.image import Image
from libqtile.widget.groupbox import GroupBox
from libqtile.widget.currentlayout import CurrentLayoutIcon
from libqtile.widget.windowname import WindowName
from libqtile.widget.spacer import Spacer
from libqtile.widget.systray import Systray
from libqtile.widget.battery import Battery
from libqtile.widget.clock import Clock

from modules.unicodes import left_half_circle, right_half_circle
from modules.colorSchemes import lushPlum

bar = Bar([
    # Sep(
    #     padding = 6,
    #     linewidth = 0,
    #     background = lushPlum["metallicViolet"]
    # ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 2
    ),
    left_half_circle(lushPlum["darkOrchid"]),
    Image(
        filename = "~/.config/qtile/icons/fedora-white.png",
        scale = "False",
        margin = 6,
        background = lushPlum["metallicViolet"],
        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi")}
    ),
    right_half_circle(lushPlum["metallicViolet"]),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 2
    ),
    left_half_circle(lushPlum["metallicViolet"]),
    GroupBox(
        font = "Ubuntu Bold",
        fontsize = 16,
        margin_y = 3,
        margin_x = 0,
        padding_y = 5,
        padding_x = 3,
        borderwidth = 3,
        active = lushPlum["babyPink"],
        inactive = lushPlum["metallicViolet"],
        rounded = False,
        highlight_color = lushPlum["currentLine"],
        highlight_method = "text",
        this_current_screen_border = lushPlum["aero"],
        #this_screen_border = colors [4],
        other_current_screen_border = lushPlum["cyan"],
        #other_screen_border = colors[4],
        #foreground = colors[2],
        background = lushPlum["darkOrchid"]
    ),
    right_half_circle(lushPlum["darkOrchid"]),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 2
    ),
    left_half_circle(lushPlum["paleViolet"]),
    CurrentLayoutIcon(
        custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
        background = lushPlum["metallicViolet"],
        padding = 0,
        scale = 0.5
    ),
    right_half_circle(lushPlum["metallicViolet"]),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 2
    ),
    left_half_circle(lushPlum["background"]),
    WindowName(
        fmt = ' {}',
        font = 'Ubuntu Bold',
        foreground = lushPlum["metallicViolet"],
        background = lushPlum["paleViolet"],
        max_chars = 999,
        padding = 0,
        fontsize = 14
    ),
    right_half_circle(lushPlum["paleViolet"]),
    
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5
    ),
    Systray(
        icon_size = 22,
        background = lushPlum["gunMetal"],
        padding = 2
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5
    ),

    left_half_circle(lushPlum["gunMetal"]),
    Battery(
        background = lushPlum["screaminGreen"],
        foreground = lushPlum["mediumOrchid"],
        charge_char = '',
        discharge_char = '',
        empty_char = '',
        fmt = '{} ',
        font = 'CaskaydiaCove Nerd Font',
        fontsize = 14,
        full_char = '',
        format = '{char} {percent:2.0%}',
        #low_percentage = 0.2,
        #low_background = dracula["red"],
        #low_foreground = 'common["white"]',
        unknown_char = '',
        update_interval = 1
    ),
    right_half_circle(lushPlum["screaminGreen"]),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 2
    ),
    left_half_circle(lushPlum["mediumOrchid"]),
    Clock(
        foreground = lushPlum["white"],
        background = lushPlum["mediumOrchid"],
        font = "CaskaydiaCove Nerd Font",
        fontsize = 14,
        #format = "  %a, %d %b ",
        format = "  %a, %d %b [ %I:%M %p ] ",
        timezone = 'Asia/Kolkata',
        update_interval = 5
    ),
    left_half_circle(lushPlum["frenchPlum"]),
    # Clock(
    #     foreground = lushPlum["classicRose"],
    #     background = lushPlum["flirt"],
    #     font = "CaskaydiaCove Nerd Font",
    #     fontsize = 14,
    #     format = " [ %I:%M %p ] ",
    #     timezone = 'Asia/Kolkata',
    #     update_interval = 5
    # ),
    # left_half_circle(lushPlum[""],lushPlum["frenchPlum"]),
    Image(
        filename = '~/.config/qtile/icons/shutdown.png',
        scale = 'False',
        margin = 6,
        background = lushPlum["frenchPlum"],
        mouse_callbacks = {
            "Button1": lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
        }
    )
],
30,
# border_width=[2, 0, 2, 0],  # Draw top and bottom borders
# border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
)