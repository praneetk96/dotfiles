# Lush Plum
def lushPlum():
    colors = [["#282A36", "#282A36"], # Background (Gun Metal) [0]
              ["#590696", "#590696"], # Metallic Violet [1]
              ["#C6A0F6", "#C6A0F6"], # Pale Violet [2]
              ["#8BE9FD", "#8BE9FD"], # Cyan [3]
              ["#7DC4E4", "#7DC4E4"], # Aero [4]
              ["#9932CC", "#9932CC"], # Dark Orchid [5]
              ["#E94435", "#E94435"], # Cinnabar [6]
              ["#50FA7B", "#50FA7B"], # Green [7]
              ["#FFBF00", "#FFBF00"], # Amber [8]
              ["#a9a1e1", "#a9a1e1"]] # Background (Dark grey) [9]
    return colors

# Dracula
def dracula():
    colors = [["#282c34", "#282c34"],
              ["#1c1f24", "#1c1f24"],
              ["#dfdfdf", "#dfdfdf"],
              ["#ff6c6b", "#ff6c6b"],
              ["#98be65", "#98be65"],
              ["#da8548", "#da8548"],
              ["#51afef", "#51afef"],
              ["#c678dd", "#c678dd"],
              ["#46d9ff", "#46d9ff"],
              ["#a9a1e1", "#a9a1e1"]]
    return colors