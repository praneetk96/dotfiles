#  ____  _  __
# |  _ \| |/ /    Praneet Kumar
# | |_) | ' /     https://gitlab.com/praneetk96
# |  __/| . \     
# |_|   |_|\_\
#
#
# Common colors
common = {
        "black": "#000000",
        "white": "#FFFFFF",
        "grey": "#555962",
        "darkSlateGrey": "#2F4858",
        "darkVanilla": "#D0B7A7",
        }

# Catppuccin Original Color Scheme but now renamed as "Mocha"
# source: "https://github.com/catppuccin/catppuccin"
# source [color palette image]: "https://raw.githubusercontent.com/catppuccin/catppuccin/main/assets/palette/macchiato.png"
mocha = {
        "champagnePink": "#F4DBD6",
        "babyPink": "#F0C6C6",
        "classicRose": "#F5BDE6",
        "paleViolet": "#C6A0F6",
        "ruddyPink": "#ED8796",
        "mauvelous": "#EE99A0",
        "lightSalmon": "#F5A97F",
        "deepChampagne": "#EED49F",
        "GrannySmithApple": "#A6DA95",
        "middleBlueGreen": "#8BD5CA",
        "cornFlower": "#91D7E3",
        "aero": "#7DC4E4",
        "jordyBlue": "#8AADF4",
        "vodka": "#B7BDF8",
        "lavenderBlue": "#C5CFF5",
        }

# Dracula Original Color Scheme
# source: "https://draculatheme.com/contribute"
dracula = {
        "background": "#282A36",
        "currentLine": "#44475A",
        "foreground": "#F8F8F2",
        "comment": "#6272A4",
        "cyan": "#8BE9FD",
        "green": "#50FA7B",
        "orange": "#FFB86C",
        "pink": "#FF79C6",
        "purple": "#BD93F9",
        "red": "#FF5555",
        "yellow": "#F1FA8C"
        }

# Joy Color Scheme
# generated on: "https://coolors.co/" --> "https://coolors.co/010819-8488d1-fee1cd-e94435-2cbe8e"
joy = {
        "richBlack": "#010819",
        "shadowBlue": "#8488D1",
        "unbleachedSilk": "#FEE1CD",
        "cinnabar": "#E94435",
        "mountainMeadow": "#2CBE8E",
        "independence": "#383B53",
        "maroonX11": "#B7245C",
        "plumWeb": "#D68FD6",
        "blueNCS": "#2E86AB",
        "jonquil": "#F9C80E",
        "amber": "#FFBF00",
        "greenBlue": "#2364AA",
        "skobeloff": "#006B80",
        "kenyanCopper": "#741602",
        "emerald": "#44DA88",
        "persianGreen": "#00B79A",
        "viridianGreen": "#009197",
        }

# LushPurp
lushPurp = {
        "academyPurple": "#525367",
        "aconitePurple": "#7249d6",
        "agedPurple": "#a442a0",
        "akebiPurple": "#983fb2",
        "alienPurple": "#490648",
        "asterPurple": "#7d74a8",
        "awkwardPurple": "#d208cc",
        "palatinatePurple": "#610C63",
        "frenchPlum": "#810955",
        "persianPink": "#EE81B3",
        "maximumBluePurple": "#A1A9FA",
        "frenchLilac": "#816797",
        "crayola": "#BD4291",
        "metallicViolet": "#590696",
        "lavenderIndigo": "#A760FF",
        "russianViolet": "#371B58",
        "flirt": "#A91079",
        "floralLavender": "#B57EDC",
        "amethyst": "#9966CC",
        "darkMagenta": "#8B008B",
        "darkViolet": "#9400D3",
        "darkOrchid": "#9932CC",
        "mediumPurple": "#9370DB",
        "mediumOrchid": "#BA55D3",
}

# My custom colorscheme "LushPlum"
lushPlum = {
    "mauve": "#E1ACFF",
    "gunMetal": "#282C34",
    "darkGunMetal": "#1D2330",
    "metallicViolet": "#590696",
    "darkOrchid": "#9932CC",
    "babyPink": "#F0C6C6",
    "currentLine": "#44475A",
    "aero": "#7DC4E4",
    "cyan": "#8BE9FD",
    "paleViolet": "#C6A0F6",
    "background": "#282A36",
    "persianPink": "#FF79C6",
    "mediumOrchid": "#BA55D3",
    "screaminGreen": "#50FA7B",
    "classicRose": "#F5BDE6",
    "white": "#FFFFFF",
    "flirt": "#A91079",
    "frenchPlum": "#810955",
}

