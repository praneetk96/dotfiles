import imp
import os
from random import uniform
from libqtile import qtile
from libqtile.bar import Bar
from libqtile.widget.sep import Sep
from libqtile.widget.image import Image
from libqtile.widget.groupbox import GroupBox
from libqtile.widget.currentlayout import CurrentLayoutIcon, CurrentLayout
from libqtile.widget.windowname import WindowName
from libqtile.widget.tasklist import TaskList
from libqtile.widget.spacer import Spacer
from libqtile.widget.systray import Systray
from libqtile.widget.battery import Battery
from libqtile.widget.clock import Clock
# from config import shortTitle

from modules.unicodes import left_arrow, right_arrow
from modules.colorSchemes import lushPlum

bar = Bar([
    Sep(
        padding = 6,
        linewidth = 0,
        background = lushPlum["metallicViolet"]
    ),
    Image(
        filename = "~/.config/qtile/icons/fedora-white.png",
        scale = "False",
        margin = 6,
        background = lushPlum["metallicViolet"],
        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi")}
    ),
    right_arrow(lushPlum["darkOrchid"],lushPlum["metallicViolet"]),
    GroupBox(
        font = "Ubuntu Bold",
        fontsize = 16,
        margin_y = 3,
        margin_x = 0,
        padding_y = 5,
        padding_x = 3,
        borderwidth = 3,
        active = lushPlum["babyPink"],
        inactive = lushPlum["metallicViolet"],
        rounded = False,
        highlight_color = lushPlum["currentLine"],
        highlight_method = "text",
        this_current_screen_border = lushPlum["aero"],
        #this_screen_border = colors [4],
        other_current_screen_border = lushPlum["cyan"],
        #other_screen_border = colors[4],
        #foreground = colors[2],
        background = lushPlum["darkOrchid"]
    ),
    right_arrow(lushPlum["metallicViolet"],lushPlum["darkOrchid"]),
    CurrentLayoutIcon(
        custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
        background = lushPlum["metallicViolet"],
        padding = 0,
        scale = 0.5
    ),
    CurrentLayout(
        background = lushPlum["metallicViolet"],
    ),
    right_arrow(lushPlum["paleViolet"],lushPlum["metallicViolet"]),
    # WindowName(
    #     fmt = ' {}',
    #     font = 'Ubuntu Bold',
    #     fontsize = 14,
    #     foreground = lushPlum["metallicViolet"],
    #     background = lushPlum["paleViolet"],
    #     max_chars = 999,
    #     padding = 0
    # ),
    WindowName(
        background = lushPlum["paleViolet"],
        empty_group_string = 'Desktop',
        fmt = ' {}',
        font = 'Ubuntu Bold',
        fontsize = 14,
        for_current_screen = True,
        foreground = lushPlum["metallicViolet"],
        # format = '{state}{name}',
        format = ' {name}',
        max_chars = 999,
        padding = 0,
    ),
    # TaskList(
    #     font = 'Ubuntu Bold',
    #     fontsize = 14,
    #     foreground = lushPlum["metallicViolet"],
    #     background = lushPlum["paleViolet"],
    #     border = lushPlum["paleViolet"],
    #     borderwidth = 1,
    #     icon_size = 0,
    #     margin = 0,
    #     # markup_focused = '<span underline="low">+{}</span>',
    #     # markup_focused = '<span> {}</span>',
    #     markup_focused = '<span>[ {} ]</span>',
    #     markup_normal = '<span>{}</span>',
    #     padding = 6,
    #     highlight_method = "block",
    #     title_width_method = "uniform",
    #     urgent_alert_method = "border",
    #     rounded = False,
    #     txt_floating = "🗗 ",
    #     txt_maximized = "🗖 ",
    #     txt_minimized = "🗕 ",
    # ),
    # TaskList(
    #     font = 'Ubuntu Bold',
    #     fontsize = 14,
    #     foreground = lushPlum["metallicViolet"],
    #     background = lushPlum["paleViolet"],
    #     border = lushPlum["paleViolet"],
    #     borderwidth = 1,
    #     icon_size = 0,
    #     margin = 0,
    #     # markup_focused = '<span underline="low">+{}</span>',
    #     # markup_focused = '<span> {}</span>',
    #     markup_focused = '<span>[ {} ]</span>',
    #     markup_normal = '<span>{}</span>',
    #     padding = 6,
    #     max_title_width = None,
    #     highlight_method = "block",
    #     title_width_method = "uniform",
    #     urgent_alert_method = "border",
    #     # parse_text = shortTitle,
    #     rounded = False,
    #     txt_floating = "🗗 ",
    #     txt_maximized = "🗖 ",
    #     txt_minimized = "🗕 ",
    # ),
    right_arrow(lushPlum["background"],lushPlum["paleViolet"]),
    
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5
    ),
    Systray(
        icon_size = 22,
        background = lushPlum["gunMetal"],
        padding = 2
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5
    ),

    left_arrow(lushPlum["gunMetal"],lushPlum["metallicViolet"]),
    Battery(
        #background = lushPlum["screaminGreen"],
        #background = lushPlum["frenchPlum"],
        background = lushPlum["metallicViolet"],
        foreground = lushPlum["babyPink"],
        charge_char = '',
        discharge_char = '',
        empty_char = '',
        fmt = '{} ',
        font = 'CaskaydiaCove Nerd Font',
        #fontsize = 14,
        full_char = '',
        format = '{char} {percent:2.0%}',
        #low_percentage = 0.2,
        #low_background = dracula["red"],
        #low_foreground = 'common["white"]',
        unknown_char = '',
        update_interval = 1
    ),
    left_arrow(lushPlum["metallicViolet"],lushPlum["darkOrchid"]),
    Clock(
        #foreground = lushPlum["white"],
        foreground = lushPlum["babyPink"],
        #background = lushPlum["mediumOrchid"],
        background = lushPlum["darkOrchid"],
        font = "CaskaydiaCove Nerd Font",
        #fontsize = 14,
        #format = "  %a, %d %b ",
        format = "  %a, %d %b [ %I:%M %p ] ",
        timezone = 'Asia/Kolkata',
        update_interval = 5
    ),
    left_arrow(lushPlum["darkOrchid"],lushPlum["metallicViolet"]),
    # Clock(
    #     foreground = lushPlum["classicRose"],
    #     background = lushPlum["flirt"],
    #     font = "CaskaydiaCove Nerd Font",
    #     fontsize = 14,
    #     format = " [ %I:%M %p ] ",
    #     timezone = 'Asia/Kolkata',
    #     update_interval = 5
    # ),
    # left_arrow(lushPlum[""],lushPlum["frenchPlum"]),
    Image(
        filename = '~/.config/qtile/icons/shutdown.png',
        scale = 'False',
        margin = 6,
        #background = lushPlum["frenchPlum"],
        background = lushPlum["metallicViolet"],
        mouse_callbacks = {
            "Button1": lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
        }
    )
],
30,
#margin = [6,6,6,6]
# border_width=[2, 0, 2, 0],  # Draw top and bottom borders
# border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
)
