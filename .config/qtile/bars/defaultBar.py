# Imports
import os
from libqtile import qtile
from libqtile.bar import Bar
from libqtile.widget.spacer import Spacer
from libqtile.widget.image import Image
from libqtile.widget.groupbox import GroupBox
from libqtile.widget.currentlayout import CurrentLayoutIcon
from libqtile.widget.currentlayout import CurrentLayout
from libqtile.widget.windowname import WindowName
from libqtile.widget.systray import Systray
from libqtile.widget.battery import Battery
from libqtile.widget.check_updates import CheckUpdates
from libqtile.widget.clock import Clock
from libqtile.widget.quick_exit import QuickExit

from qtile_extras.widget.decorations import PowerLineDecoration 
from qtile_extras.widget.decorations import RectDecoration

# from config import terminal
from modules.colorSchemes import *

terminal = "alacritty"

# Qtile Extras widget configs
powerline_left = {
    "decorations": [
        PowerLineDecoration()
    ]
}
powerline_right = {
    "decorations": [
        PowerLineDecoration(
            path="arrow_right"
        )
    ]
}

rounded_left = {
    "decorations": [
        PowerLineDecoration(
            path = "rounded_left"
        )
    ]
}
rounded_right = {
    "decorations": [
        PowerLineDecoration(
            path = "rounded_right"
        )
    ]
}

bar = Bar([
    Spacer(
        background = lushPlum["metallicViolet"],
        length = 5
    ),
    Image(
        filename = "~/.config/qtile/icons/fedora-white.png",
        scale = "False",
        margin = 6,
        background = lushPlum["metallicViolet"],
        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi")},
    ),
    Spacer(
        background = lushPlum["metallicViolet"],
        length = 5
    ),
    GroupBox(
        font = "Ubuntu Bold",
        fontsize = 16,
        margin_y = 3,
        margin_x = 5,
        padding_y = 5,
        padding_x = 3,
        borderwidth = 3,
        active = lushPlum["paleViolet"],
        inactive = lushPlum["metallicViolet"],
        rounded = False,
        highlight_color = dracula["cyan"],
        highlight_method = "text",
        this_current_screen_border = lushPlum["aero"],
        other_current_screen_border = lushPlum["cyan"],
        background = lushPlum["metallicViolet"],
        decorations = [
                RectDecoration(
                    colour = lushPlum["darkOrchid"],
                    radius = 10, 
                    filled = True, 
                    padding_y = 4, 
                    group = True
                )
        ],
        padding = 10,
    ),
    Spacer(
        background = lushPlum["metallicViolet"],
        length = 5
    ),
    CurrentLayoutIcon(
        custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
        background = lushPlum["metallicViolet"],
        padding = 5,
        scale = 0.5,
        use_mask = True,          # Use only when 'widget' is imported from qtile_extras
        foreground = lushPlum["paleViolet"],
    ),
    CurrentLayout(
        background = lushPlum["metallicViolet"],
        # **powerline_left,
        **rounded_left,
    ),
    WindowName(
        background = lushPlum["paleViolet"],
        empty_group_string = 'Desktop',
        fmt = ' {}',
        font = 'Ubuntu Bold',
        fontsize = 14,
        for_current_screen = True,
        foreground = lushPlum["metallicViolet"],
        format = ' {name}',
        max_chars = 95,
        padding = 0,
        # **powerline_left,
        **rounded_left
    ),
    
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5
    ),
    Systray(
        icon_size = 22,
        background = lushPlum["gunMetal"],
        padding = 2
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5,
    ),
    Battery(
        background = lushPlum["gunMetal"],
        foreground = lushPlum["gunMetal"],
        charge_char = '',
        discharge_char = '',
        empty_char = '',
        fmt = '{} ',
        font = 'CaskaydiaCove Nerd Font',
        fontsize = 14,
        full_char = '',
        format = '{char} {percent:2.0%}',
        unknown_char = '',
        update_interval = 1,
        decorations = [
                RectDecoration(
                    colour = dracula["cyan"],
                    radius = 10, 
                    filled = True, 
                    padding_y = 4, 
                    group = True
                )
        ],
        padding = 10,
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5,
    ),
    CheckUpdates(
        background = lushPlum["gunMetal"],
        colour_have_updates = lushPlum["background"],
        colour_no_updates = lushPlum["background"],
        display_format = ' Updates: {updates}',
        distro = 'Fedora',
        fmt = '{}',
        font = 'CaskaydiaCove Nerd Font',
        fontsize = 14,
        foreground = joy["cinnabar"],
        initial_text = '',
        no_update_string = ' No updates',
        update_interval = 1800,    
        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo dnf update')},
        decorations = [
                RectDecoration(
                    colour = dracula["green"],
                    radius = 10, 
                    filled = True, 
                    padding_y = 4, 
                    group = True
                )
        ],
        padding = 10,
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5,
    ),
    Clock(    
        foreground = lushPlum["background"],
        background = lushPlum["background"],
        font = "CaskaydiaCove Nerd Font",
        format = "  %a, %d %b [ %I:%M %p ]",
        timezone = 'Asia/Kolkata',
        update_interval = 5,
        decorations = [
                RectDecoration(
                    colour = joy["amber"],
                    radius = 10, 
                    filled = True, 
                    padding_y = 4, 
                    group = True
                )
        ],
        padding = 10,
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5,
    ),
    QuickExit(
        background = lushPlum["background"],
        countdown_format = '  {} secs',
        countdown_start = 5,
        default_text = '襤 shutdown',
        fmt = '{} ',
        font = "CaskaydiaCove Nerd Font",
        foreground = lushPlum["background"],
        width = 115,
        timer_interval = 1,
        decorations = [
                RectDecoration(
                    colour = joy["cinnabar"],
                    radius = 10,
                    filled = True,
                    padding_y = 4,
                    group = True
                )
        ],
        padding = 10,
    ),
    Spacer(
        background = lushPlum["gunMetal"],
        length = 5,
    ),          
],30)
