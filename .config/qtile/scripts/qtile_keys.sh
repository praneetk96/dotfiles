#!/bin/sh
#
sed -n '/START_KEYS/,/END_KEYS/p' ~/.config/qtile/config.py | \
    grep -e 'Key(\[' \
    -e 'KB_GROUP' | \
    sed -e 's/^[ \t]*//' \
    -e 's/Key//' \
    -e 's/# KB_GROUP /\n/' \
    -e 's/, "/ + /' \
    -e 's/"], "/ + /' \
    -e 's/", /]\t: /' | \
    # yad --text-info --title "Qtile Keybindings" --text="Qtile Keybindings" --text-align=center --borders=10 --on-top --lang=python --back=#282c34 --fore=#46d9ff --geometry=1200x720 --margins=30 --formatted='<span>font_desc="Sans 200"</span>' --no-buttons
    yad --text-info --title "Qtile Keybindings" --text="Qtile Keybindings" --text-align=center --borders=10 --on-top  --back=#282c34 --fore=#46d9ff --geometry=1200x720 --margins=30 --formatted='<span>font_desc="Sans 200"</span>' --no-buttons
