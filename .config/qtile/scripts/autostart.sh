#!/bin/sh

### DISABLED ###
# picom &
# picom -f &
# picom --config ~/.config/picom/picom.conf &
# picom --experimental-backends --vsync -f &
# /usr/lib64/xfce4/notifyd/xfce4-notifyd &
# /usr/local/bin/betterlockscreen -u Pictures/WallHaven/wallhaven-4ow3w9.jpg &
# mpv --no-video ~/.config/qtile/audio/startup-01.mp3 &
# volumeicon &
# blueberry-tray &
# xfce4-clipman &
# pasystray &
# udiskie -t &
# xfce4-volumed-pulse &

### ENABLED ###
#picom --unredir-if-possible --backend=glx --vsync &
#picom --unredir-if-possible --backend=glx --vsync --blur-method dual_kawase &
#notify-log .log/notify.log &
picom --unredir-if-possible --backend=glx --vsync --blur-method dual_kawase --blur-strength=3 &
nitrogen --restore &
dunst &
nm-applet &
pasystray &
redshift-gtk &
thunar --daemon &
xfce4-power-manager &
/usr/bin/lxpolkit &
lxsession -n -a &
/usr/local/bin/betterlockscreen -u Pictures/WallHaven/wallhaven-g78omd.png &
