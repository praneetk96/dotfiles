#!/bin/sh

# Tasks to be executed before extiting qtile session
killall dunst &
killall redshift-gtk &
killall redshift-gtk &
killall udiskie &
killall picom &
killall xfce4-power-manager &
killall xfce4-volumed-pulse &
killall lxsession &
killall pasystray &
killall xfce4-volumed-pulse &
killall blueberry-tray &
# mpv --no-video ~/.config/qtile/audio/shutdown-01.mp3
