# ____  _  __
#|  _ \| |/ /
#| |_) | ' /
#|  __/| . \
#|_|   |_|\_\
#
# This is my qtile's wm config file
#
import os
import re
import socket
import subprocess
from turtle import position
from libqtile import bar, layout, widget, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.extension.command_set import CommandSet
from libqtile.extension.dmenu import DmenuRun
from libqtile.extension.window_list import WindowList
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from modules.colorSchemes import *

from libqtile.bar import CALCULATED

# For unicode symbols in bar
from typing import Optional
from libqtile.widget.textbox import TextBox

from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration
from qtile_extras.widget.decorations import PowerLineDecoration
from qtile_extras.widget.decorations import RectDecoration
from qtile_extras.resources.wallpapers import *

# ######from modules.bar_arrow import bar
#from bar_arrow import bar

#from modules.bar_halfCircle import bar

#from libqtile.utils import guess_terminal

import colors

alt = "mod1"
mod = "mod4"
# terminal = "st"
# terminal = "urxvt"
terminal = "alacritty"
# terminal = "kitty"
# terminal = "xfce4-terminal"
browser = "firefox"
privateBrowser = "firefox --private-window"
# browser = "brave-browser"
# privateBrowser = "brave-browser --incognito"
dmenu = "dmenu_run -c -l 15 -nf '#560696' -nb '#C6A0F6' -sb '#590696' -sf '#C6A0F6' -fn 'caskaydiaCove-14' -p Search: "
# dmenu = "dmenu_run -c -l 20"
# dmenu = "dmenu_run"
##WITHOUT PATCH##dmenu = "dmenu_run -b -nf '#560696' -nb '#C6A0F6' -sb '#590696' -sf '#C6A0F6' -fn 'caskaydiaCove-14' -p 'Search: '"
# dmenu = "dmenu_run -b -nf '#F8F8F2' -nb '#282A36' -sb '#6272A4' -sf '#F8F8F2' -fn 'caskaydiaCove-14' -p 'Search: '"
# dmenu = """j4-dmenu-desktop --display-binary --dmenu='dmenu_run -l 10 -b -nf "#F8F8F2" -nb "#282A36" -sb "#6272A4" -sf "#F8F8F2" -fn "monospace-14" -p "Search: "'"""
rofi = "rofi -show drun"
screenshot = "xfce4-screenshooter"
email = "thunderbird"
#fileManager = "pcmanfm"
fileManager = "thunar"
lockScreen = "betterlockscreen -l dimblur"
#powerMenu = "/home/gideon/.config/qtile/scripts/power.sh"
powerMenu = "/home/gideon/.config/rofi/powermenu.sh"
# powerMenu = "/home/gideon/.config/rofi/powermenu.rasi"
# appLauncher = "rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/slingshot.rasi"
appLauncher = "rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi"
appSwh = "rofi -show window"
emoji = "rofimoji"
qtileKeys = "/home/gideon/.config/qtile/scripts/qtile_keys.sh"
# notifications = "alacritty -e nvim /home/gideon/.log/notify.log"
notifications = "alacritty -e vim /home/gideon/.log/notify.log"

# SYSTEM-SOUNDS
startupSound = "mpv --no-video /home/gideon/.config/qtile/audio/startup-01.mp3"
shutdownSound = "mpv --no-video /home/gideon/.config/qtile/audio/shutdown-01.mp3"
menuSound = "mpv --no-video /home/gideon/.config/qtile/audio/menu-01.mp3"
alertSound = "mpv --no-video /home/gideon/.config/qtile/audio/alert.mp3"
alertSoundAlt = "mpv --no-video /home/gideon/.config/qtile/audio/menu-02.mp3"

# START_KEYS
keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # KB_GROUP Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # KB_GROUP Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # KB_GROUP Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),

    # KB_GROUP Custom key bindings for my applications
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch browser"),
    Key([mod, "shift"], "b", lazy.spawn(privateBrowser), desc="Launch private browser"),
    Key([mod], "m", lazy.spawn(email), desc="Launch email-client"),
    Key([mod], "e", lazy.spawn(fileManager), desc="Launch file-manager"),
    Key([mod], "p", lazy.spawn(dmenu), desc="Launch dmenu"),
    Key([mod], "r", lazy.spawn(dmenu), desc="Launch patched dmenu"),
    Key([mod], "Print", lazy.spawn(screenshot), desc="Launch screenshot app"),
    Key([mod], "x", lazy.spawn(lockScreen), desc="Launch and lock the screen"),
    Key([mod, "shift"], "e", lazy.spawn(emoji), desc="Launch rofi emoji"),
    Key([mod], "q", lazy.spawn(powerMenu), desc="Launch rofi powermenu script"),
    Key([mod], "a", lazy.spawn(appLauncher), desc="Launch rofi app launcher"),
    Key([], "F1", lazy.spawn(qtileKeys), desc="Launch this help"),
    Key([], "F3", lazy.spawn(notifications), desc="Launch notifications"),
    
    # KB_GROUP WindowList extension, switch between all windows
    Key([alt], 'Tab', lazy.run_extension(WindowList(
        all_groups = True,
        dmenu_bottom = True,
        font = 'CaskaydiaCove Nerd Font',
        fontsize = "14",
        dmenu_prompt = " ",
        dmenu_height = 10,
        background = '#C6A0F6',
        foreground = '#560696',
        selected_foreground = '#C6A0F6',
        selected_background = '#590696',
    ))),
    
    # KB_GROUP Dmenu options to edit files
    Key([mod, "control"], "e", lazy.run_extension(CommandSet(
        commands = {
            "Qtile's config": 'alacritty -e nvim /home/gideon/.config/qtile/config.py',
            'Bashrc': 'alacritty -e nvim /home/gideon/.bashrc',
        },
        dmenu_bottom = True,
        background = '#C6A0F6',
        foreground = '#560696',
        dmenu_prompt = ' ',
        dmenu_lines = 10,
        dmenu_height = 10,
        selected_foreground = '#C6A0F6',
        selected_background = '#590696',
    ))),

    # KB_GROUP Toggle Fullscreen
    Key([mod], "f", lazy.window.toggle_fullscreen(), lazy.hide_show_bar(position='all'), desc='Toggle fullscreen and the bars'),

    # KB_GROUP Toggle Floating
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='Toggle Floating'),
    
    # KB_GROUP Control monitor backlight from keyboard
    # Key([], "XF86MonBrightnessDown", lazy.spawn(os.path.expanduser("~/.config/scripts/brightDown.sh")), desc="Decrease brightness"),
    # Key([], "XF86MonBrightnessUp", lazy.spawn(os.path.expanduser("~/.config/scripts/brightUp.sh")), desc="Increase brightness"),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 5%-")), # light -U 5 OR brightnessctl set 5%-
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +5%")), # light -A 5 OR brightnessctl set +5%
    
    # KB_GROUP Control volume from keyboard
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),

    # KB_GROUP Control media from keyboard
    # Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Play the stopped or paused media"),
    # Key([], "XF86AudioPause", lazy.spawn("playerctl play-pause"), desc="Pause the playing media"),
    # Key([], "XF86AudioStop", lazy.spawn("playerctl stop"), desc="Stop the playing media"),
    # Key([], "XF86AudioPrev", lmediaazy.spawn("playerctl previous"), desc="Change to previous track/media"),
    # Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Change to next track/media"),
    
    # KB_GROUP Control deadbeef from keyboard
    Key([], "XF86AudioPlay", lazy.spawn("deadbeef --play-pause"), desc="Play the stopped or paused media"),
    Key([], "XF86AudioPause", lazy.spawn("deadbeef --play-pause"), desc="Pause the playing media"),
    Key([], "XF86AudioStop", lazy.spawn("deadbeef --stop"), desc="Stop the playing media"),
    Key([], "XF86AudioPrev", lazy.spawn("deadbeef --prev"), desc="Change to previous track/media"),
    Key([], "XF86AudioNext", lazy.spawn("deadbeef --next"), desc="Change to next track/media"),

    # KB_GROUP Play an alert sound when caps, num, scroll lock is pressed
    Key([], "Caps_Lock", lazy.spawn(alertSoundAlt), desc = "Play sound when Caps lock is pressed"),
    Key([], "Num_Lock", lazy.spawn(alertSoundAlt), desc = "Play sound when Num lock is pressed"),
    Key([], "Scroll_Lock", lazy.spawn(alertSoundAlt), desc = "Play sound when Scroll lock is pressed"),

    # KB_GROUP Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
]
# END_KEYS

# Disabled keys
#Key([mod, "shift"], "Return", lazy.spawn(rofi), desc="Launch rofi"),
#Key([mod], "r", lazy.spawn(rofi), desc="Launch rofi"),
# Key([mod], "a", lazy.spawn(appLauncher), desc="Launch rofi app launcher"),
# Key([alt], "Tab", lazy.spawn(appSwh), desc="Launch rofi window switcher"),
#Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
# Toggle bars
    #Key([mod], "z", lazy.hide_show_bar(position='all'), desc="Toggle bars"),
    
    # Key([mod], 'v', lazy.run_extension(extension.CommandSet(
    #     commands={
    #         'play/pause': '[ $(mocp -i | wc -l) -lt 2 ] && mocp -p || mocp -G',
    #         'next': 'mocp -f',
    #         'previous': 'mocp -r',
    #         'quit': 'mocp -x',
    #         'open': 'alacritty -e mocp',
    #         'shuffle': 'mocp -t shuffle',
    #         'repeat': 'mocp -t repeat',
    #     },
    #     pre_commands=['[ $(mocp -i | wc -l) -lt 1 ] && mocp -S'],))),

    # Don't use this one(command-line mixer for ALSA soundcard driver)
    # Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    # Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 sset Master 1- unmute")),
    # Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 sset Master 1+ unmute")),    

    # Use this(Control a running PulseAudio sound server)
    ###Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
    ###Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    ###Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),

    # Control Screen Brightness from keyboard
    # Don't use this one
    #Key([], "XF86MonBrightnessDown", lazy.spawn(brightDown)), # light -U 5 OR brightnessctl set 5%-
    #Key([], "XF86MonBrightnessUp", lazy.spawn(brightUp)), # light -A 5 OR brightnessctl set +5%

    # Use this
    #Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 5%-")), # light -U 5 OR brightnessctl set 5%-
    #Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +5%")), # light -A 5 OR brightnessctl set +5%

groups = [
    Group("1", label="", layout='monadtall', matches=[Match(wm_class=["firefox", "Transmission-gtk", "librewolf-default", "brave-browser"])]),
    Group("2", label="", layout='monadtall', matches=[Match(wm_class=["VSCodium", "Code", "lapce"])]),
    Group("3", label="", layout='monadtall', matches=[Match(wm_class=["GParted", "Grub-customizer", "Lxappearance"])]),
    Group("4", label="", layout='monadtall', matches=[Match(wm_class=["libreoffice"])]),
    Group("5", label="", layout='monadtall', matches=[Match(wm_class=["VirtualBox Manager", "dosbox-staging", "heroic", "Steam"])]),
    Group("6", label="", layout='monadtall', matches=[Match(wm_class=["Thunderbird", "thunderbird","TelegramDesktop","Signal", "Gfeeds"])]),
    Group("7", label="", layout='monadtall', matches=[Match(wm_class=["Audacious", "Deadbeef", "Tauon Music Box", "Audacity", "Lollypop"])]),
    Group("8", label="", layout='monadtall', matches=[Match(wm_class=["Parole", "vlc", "obs", "SimpleScreenRecorder"])]),
    Group("9", label="", layout='floating', matches=[Match(wm_class=["Inkscape", "Gimp-2.10"])])
]

# groups = [
#     Group("1", label="", layout='monadtall', matches=[Match(wm_class=["firefox", "Transmission-gtk", "librewolf-default", "brave-browser"])]),
#     Group("2", label="", layout='monadtall', matches=[Match(wm_class=["VSCodium", "Code"])]),
#     Group("3", label="", layout='monadtall', matches=[Match(wm_class=["GParted", "Grub-customizer", "Lxappearance"])]),
#     Group("4", label="", layout='monadtall', matches=[Match(wm_class=["libreoffice"])]),
#     Group("5", label="", layout='monadtall', matches=[Match(wm_class=["VirtualBox Manager", "dosbox-staging", "heroic", "Steam"])]),
#     Group("6", label="", layout='monadtall', matches=[Match(wm_class=["Thunderbird", "thunderbird","TelegramDesktop","Signal", "Gfeeds"])]),
#     Group("7", label="", layout='monadtall', matches=[Match(wm_class=["Audacious", "Deadbeef", "Tauon Music Box", "Audacity", "Lollypop"])]),
#     Group("8", label="", layout='monadtall', matches=[Match(wm_class=["Parole", "vlc"])]),
#     Group("9", label="", layout='floating', matches=[Match(wm_class=["Inkscape", "Gimp-2.10"])])
# ]

# groups = [
#     Group("1", label="一", layout='monadtall', matches=[Match(wm_class=["firefox", "Transmission-gtk", "librewolf-default", "brave-browser"])]),
#     Group("2", label="二", layout='monadtall', matches=[Match(wm_class=["VSCodium", "Code"])]),
#     Group("3", label="三", layout='monadtall', matches=[Match(wm_class=["GParted", "Grub-customizer", "Lxappearance"])]),
#     Group("4", label="四", layout='monadtall', matches=[Match(wm_class=["libreoffice"])]),
#     Group("5", label="五", layout='monadtall', matches=[Match(wm_class=["VirtualBox Manager", "dosbox-staging", "heroic", "Steam"])]),
#     Group("6", label="六", layout='monadtall', matches=[Match(wm_class=["Thunderbird", "thunderbird","TelegramDesktop","Signal", "Gfeeds"])]),
#     Group("7", label="七", layout='monadtall', matches=[Match(wm_class=["Audacious", "Deadbeef", "Tauon Music Box", "Audacity", "Lollypop"])]),
#     Group("8", label="八", layout='monadtall', matches=[Match(wm_class=["Parole", "vlc"])]),
#     Group("9", label="九", layout='floating', matches=[Match(wm_class=["Inkscape", "Gimp-2.10"])])
# ]

# groups = [
#     Group("1", label="क", layout='monadtall', matches=[Match(wm_class=["firefox", "Transmission-gtk", "librewolf-default"])]),
#     Group("2", label="ख", layout='monadtall', matches=[Match(wm_class=["VSCodium", "Code"])]),
#     Group("3", label="ग", layout='monadtall', matches=[Match(wm_class=["GParted", "Grub-customizer", "Lxappearance"])]),
#     Group("4", label="घ", layout='monadtall', matches=[Match(wm_class=["libreoffice"])]),
#     Group("5", label="ङ", layout='monadtall', matches=[Match(wm_class=["VirtualBox Manager", "dosbox-staging", "heroic", "Steam"])]),
#     Group("6", label="च", layout='monadtall', matches=[Match(wm_class=["Thunderbird", "thunderbird","TelegramDesktop"])]),
#     Group("7", label="छ", layout='monadtall', matches=[Match(wm_class=["Audacious", "Deadbeef", "Tauon Music Box", "Audacity"])]),
#     Group("8", label="ज", layout='monadtall', matches=[Match(wm_class=["Parole", "vlc"])]),
#     Group("9", label="झ", layout='floating', matches=[Match(wm_class=["Inkscape", "Gimp-2.10"])])
# ]

# groups = [
#     Group("1", label="१", layout='monadtall', matches=[Match(wm_class=["firefox", "Transmission-gtk", "librewolf-default"])]),
#     Group("2", label="२", layout='monadtall', matches=[Match(wm_class=["VSCodium", "Code"])]),
#     Group("3", label="३", layout='monadtall', matches=[Match(wm_class=["GParted", "Grub-customizer", "Lxappearance"])]),
#     Group("4", label="४", layout='monadtall', matches=[Match(wm_class=["libreoffice"])]),
#     Group("5", label="५", layout='monadtall', matches=[Match(wm_class=["VirtualBox Manager", "dosbox-staging", "heroic", "Steam"])]),
#     Group("6", label="६", layout='monadtall', matches=[Match(wm_class=["Thunderbird", "thunderbird","TelegramDesktop"])]),
#     Group("7", label="७", layout='monadtall', matches=[Match(wm_class=["Audacious", "Deadbeef", "Tauon Music Box", "Audacity"])]),
#     Group("8", label="८", layout='monadtall', matches=[Match(wm_class=["Parole", "vlc"])]),
#     Group("9", label="९", layout='floating', matches=[Match(wm_class=["Inkscape", "Gimp-2.10"])])
# ]

for i in groups:
   keys.extend(
       [
           # mod1 + letter of group = switch to group
           Key(
               [mod],
               i.name,
               lazy.group[i.name].toscreen(),
               desc="Switch to group {}".format(i.name),
           ),
           # mod1 + shift + letter of group = switch to & move focused window to group
           Key(
               [mod, "shift"],
               i.name,
               lazy.window.togroup(i.name, switch_group=True),
               desc="Switch to & move focused window to group {}".format(i.name),
           ),
           # Or, use below if you prefer not to switch to that group.
           # # mod1 + shift + letter of group = move focused window to group
           # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
           #     desc="move focused window to group {}".format(i.name)),
       ]
   )

### Custom Functions ###
# When application launched automatically focus it's group
@hook.subscribe.client_new
def modify_window(client):
    for group in groups:  # follow on auto-move
        match = next((m for m in group.matches if m.compare(client)), None)
        if match:
            targetgroup = client.qtile.groups_map[group.name]  # there can be multiple instances of a group
            targetgroup.cmd_toscreen(toggle=False)
            break

# Function to parse and modify window names
def shortTitle(text):
    for string in [" - Chromium", " - Firefox"]:
        text = text.replace(string, "")
        return text

# # A small function to toggle qtile's bar visibility based on if currentLayout is MAX or not
# def toggle_bar(qtile):
#     global bar_is_hidden
#     bar = qtile.current_screen.top
#     current_layout = qtile.current_layout
#     if current_layout.name == "Max":
#         # bar.size = 0
#         # bar.window.hide()
#         bar.show(False)
#     else:
#         bar.show(True)
#         bar.size = 30
#         bar.window.unhide()
#         qtile.currentGroup.layoutAll()

colors = colors.lushPlum()

# # pywall
# colors = []
# cache='/home/gideon/.cache/wal/colors'
# def load_colors(cache):
#     with open(cache, 'r') as file:
#         for i in range(8):
#             colors.append(file.readline().strip())
#     colors.append('#ffffff')
#     lazy.reload()
# load_colors(cache)

# # LAYOUT Themes
layout_theme = {"border_width": 2,
                # "margin": 12,
                "margin": 8,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

single_border_width_theme = {"border_width": 2,
                             "margin": 12,
                             "border_focus": "e1acff",
                             "border_normal": "1D2330",
                             "single_border_width": 0,
                             "single_margin": 0
                             }

layouts = [
    # layout.MonadTall(**single_border_width_theme),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(**single_border_width_theme),
    layout.MonadWide(**layout_theme),
    layout.Floating(**layout_theme),
    layout.Max(**layout_theme),
    
    #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(**layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.Tile(**layout_theme),
    # layout.TreeTab(*layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Zoomy(**layout_theme),
]

widget_defaults = dict(
    font = "CaskaydiaCove Nerd Font Mono",
    fontsize = 16,
    padding = 2,
)
extension_defaults = widget_defaults.copy()

# Qtile Extras widget configs
powerline_left = {
    "decorations": [
        PowerLineDecoration()
    ]
}
powerline_right = {
    "decorations": [
        PowerLineDecoration(
            path="arrow_right"
        )
    ]
}

rounded_left = {
    "decorations": [
        PowerLineDecoration(
            path = "rounded_left"
        )
    ]
}
rounded_right = {
    "decorations": [
        PowerLineDecoration(
            path = "rounded_right"
        )
    ]
}

# Extending qtile's functionality
# def getVol():
#     # Execute the script and get its output
#     vol = subprocess.check_output(["/home/gideon/.local/bin/getCurrentVolume"])
#     # Convert the output to a string and remove any leading/trailing whitespace
#     vol_output = vol.decode().strip()
#     return vol_output

# vol_widget = qtile.widget.TextBox(text=getVol())
# bar.add(vol_widget)

# def getLum():
#     # Execute the script and get its output
#     lum = subprocess.check_output(["/home/gideon/.local/bin/getCurrentBrightness"])
#     # Convert the output to a string and remove any leading/trailing whitespace
#     lum_output = lum.decode().strip()
#     return lum_output

# WIDGETS
# Screen section is in 'bars' folder so that i can easily switch to different themed bar
screens = [
    Screen(
        top=bar.Bar(
        # bottom=bar.Bar(
            [
                # widget.Spacer(
                #     background = colors[1],
                #     length = 5
                # ),
                # widget.Image(
                #     filename = "~/.config/qtile/icons/fedora-white.png",
                #     scale = "False",
                #     margin = 6,
                #     background = colors[1],
                #     mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi")},
                # ),
                # widget.Spacer(
                #     background = colors[1],
                #     length = 5
                # ),
                widget.Spacer(
                    background = colors[0],
                    length = 5
                ),
                # widget.Wallpaper(
                #     foreground = colors[2],
                #     background = colors[0],
                #     directory = '~/Pictures/WallHaven/',
                #     fmt = '{}',
                #     font = 'CaskaydiaCove Nerd Font',
                #     fontsize = 14,
                #     # label = ' ',
                #     # label = ' ',
                #     label = ' ',
                #     option = 'fill',                 # fill or stretch
                #     random_selection = False,
                #     #random_selection = True,
                #     wallpaper_command = ['feh', '--bg-fill'],
                #     decorations = [
                #             RectDecoration(
                #                 # colour = colors[5],
                #                 colour = colors[1],
                #                 radius = 10, 
                #                 filled = True, 
                #                 padding_y = 4,
                #                 # padding_y = 7,
                #                 group = True
                #             )
                #     ],
                #     padding = 10,
                # ),
                # widget.Spacer(
                #     background = colors[0],
                #     length = 5,
                # ),
                widget.Image(
                    filename = "~/.config/qtile/icons/fedora-white.png",
                    # filename = "~/.config/qtile/icons/qtile.png",
                    # filename = "~/.config/qtile/icons/qtile-alt.png",
                    # scale = False,
                    scale = True,
                    # margin = 6,
                    margin = 7,
                    background = colors[0],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/appMenu.rasi")},
                    # decorations = [
                    #         RectDecoration(
                    #             # colour = colors[1],
                    #             # colour = "#2982BD",
                    #             radius = 10, 
                    #             filled = True, 
                    #             # padding_y = 4,
                    #             padding_y = 7,
                    #             group = True
                    #         )
                    # ],
                    padding = 6,
                    # padding = 1,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5
                ),
                # widget.GroupBox(
                #     # font = "Ubuntu Bold",
                #     font = 'CaskaydiaCove Nerd Font Bold',
                #     fontsize = 16,
                #     margin_y = 3,
                #     margin_x = 5,
                #     padding_y = 5,
                #     # padding_x = 3,
                #     padding_x = 7,
                #     borderwidth = 3,
                #     active = colors[2],
                #     inactive = colors[1],
                #     rounded = False,
                #     highlight_color = colors[3],
                #     highlight_method = "text",
                #     this_current_screen_border = colors[4],
                #     other_current_screen_border = colors[3],
                #     background = colors[1],
                #     decorations = [
                #             RectDecoration(
                #                 colour = colors[5],
                #                 radius = 10, 
                #                 filled = True, 
                #                 padding_y = 4, 
                #                 group = True
                #             )
                #     ],
                #     padding = 10,
                # ),
                # widget.Spacer(
                #     background = colors[1],
                #     length = 5
                # ),
                # widget.CurrentLayoutIcon(
                #     custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                #     background = colors[1],
                #     padding = 5,
                #     scale = 0.5,
                #     use_mask = True,          # Use only when 'widget' is imported from qtile_extras
                #     foreground = colors[2],
                # ),
                # widget.CurrentLayout(
                #     background = colors[1],
                #     # **powerline_left,
                #     **rounded_left,
                # ),
                widget.GroupBox(
                    # font = "Ubuntu Bold",
                    font = 'CaskaydiaCove Nerd Font Bold',
                    fontsize = 16,
                    margin_y = 3,
                    margin_x = 5,
                    padding_y = 5,
                    # padding_x = 3,
                    padding_x = 7,
                    borderwidth = 3,
                    active = colors[2],
                    inactive = colors[1],
                    rounded = False,
                    highlight_color = colors[3],
                    highlight_method = "text",
                    this_current_screen_border = colors[4],
                    other_current_screen_border = colors[3],
                    background = colors[0],
                    decorations = [
                            RectDecoration(
                                colour = colors[5],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 10,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5
                ),
                widget.CurrentLayoutIcon(
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                    background = colors[0],
                    # padding = 5,
                    scale = 0.5,
                    use_mask = True,          # Use only when 'widget' is imported from qtile_extras
                    foreground = colors[2],
                    decorations = [
                            RectDecoration(
                                colour = colors[1],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 6,
                ),
                widget.CurrentLayout(
                    background = colors[0],
                    decorations = [
                            RectDecoration(
                                colour = colors[1],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 6,
                ),
                # widget.Spacer(
                #     background = colors[2],
                #     length = 300,
                # ),
                # widget.WindowName(
                #     background = colors[2],
                #     empty_group_string = 'Desktop',
                #     fmt = ' {}',
                #     font = 'Ubuntu Bold',
                #     fontsize = 14,
                #     for_current_screen = True,
                #     foreground = colors[1],
                #     format = ' {name}',
                #     max_chars = 95,
                #     padding = 0,
                #     # **powerline_left,
                #     **rounded_left
                # ),
                
                widget.Spacer(
                    background = colors[0],
                ),
                widget.WindowName(
                    background = colors[0],
                    empty_group_string = 'Desktop',
                    fmt = ' {}',
                    font = 'Ubuntu Bold',
                    fontsize = 14,
                    for_current_screen = True,
                    foreground = colors[2],
                    format = ' {name}',
                    # max_chars = 95,
                    max_chars = 50,
                    width = CALCULATED,
                    padding = 0,
                ),
                widget.Spacer(
                    background = colors[0],
                ),
    
                widget.Spacer(
                    background = colors[0],
                    length = 5
                ),
                widget.Systray(
                    icon_size = 22,
                    background = colors[0],
                    padding = 2
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5,
                ),
                
                # widget.Wallpaper(
                #     foreground = colors[0],
                #     background = colors[0],
                #     directory = '~/Pictures/WallHaven/',
                #     fmt = '{}',
                #     font = 'CaskaydiaCove Nerd Font',
                #     fontsize = 14,
                #     # label = ' ',
                #     label = ' ',
                #     option = 'fill',                 # fill or stretch
                #     random_selection = True,
                #     wallpaper_command = ['feh', '--bg-fill'],
                #     decorations = [
                #             RectDecoration(
                #                 colour = colors[5],
                #                 radius = 10, 
                #                 filled = True, 
                #                 padding_y = 4, 
                #                 group = True
                #             )
                #     ],
                #     padding = 10,
                # ),
                # widget.Spacer(
                #     background = colors[0],
                #     length = 5,
                # ),
                widget.Battery(
                    background = colors[0],
                    foreground = colors[0],
                    charge_char = '',
                    discharge_char = '',
                    empty_char = '',
                    fmt = '{} ',
                    font = 'CaskaydiaCove Nerd Font',
                    fontsize = 14,
                    full_char = '',
                    format = '{char} {percent:2.0%}',
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('xfce4-power-manager-settings')},
                    unknown_char = '',
                    update_interval = 1,
                    decorations = [
                            RectDecoration(
                                colour = colors[3],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 10,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5,
                ),
                # widget.TextBox(text=getVol),
                # widget.Spacer(
                #     background = colors[0],
                #     length = 5,
                # ),
                # widget.CheckUpdates(
                #     background = colors[0],
                #     colour_have_updates = colors[0],
                #     colour_no_updates = colors[0],
                #     # display_format = ' Updates: {updates}',
                #     display_format = ' Updates:{updates}',
                #     distro = 'Fedora',
                #     fmt = '{}',
                #     font = 'CaskaydiaCove Nerd Font',
                #     fontsize = 14,
                #     foreground = colors[6],
                #     initial_text = '',
                #     # no_update_string = ' No updates',
                #     # no_update_string = '',
                #     no_update_string = ': ',
                #     update_interval = 1800,    
                #     mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo dnf update')},
                #     decorations = [
                #             RectDecoration(
                #                 colour = colors[7],
                #                 radius = 10, 
                #                 filled = True, 
                #                 padding_y = 4, 
                #                 group = True
                #             )
                #     ],
                #     padding = 10,
                # ),
                # widget.Spacer(
                #     background = colors[0],
                #     length = 5,
                # ),
                widget.Backlight(
                        background = colors[0],
                        foreground = colors[0],
                        backlight_name = 'eDP-1',
                        brightness_file = '/sys/class/backlight/intel_backlight/brightness',
                        font = 'CaskaydiaCove Nerd Font',
                        fontsize = 14,
                        # format = ' [{percent:2.0%}]',
                        format = 'lum:{percent:2.0%}',
                        max_brightness_file = '/sys/class/backlight/intel_backlight/max_brightness',
                        # padding = 5,
                        step = 10,
                        update_interval = 0.2,
                        decorations = [
                            RectDecoration(
                                colour = mocha["classicRose"],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                        ],
                        padding = 10,
                ),
                widget.Volume(
                        # foreground = lushPurp["frenchPlum"],
                        # background = mocha["classicRose"],
                        background = colors[0],
                        foreground = colors[0],
                        font = 'CaskaydiaCove Nerd Font',
                        fontsize = 14,
                        # fmt=' [{}]',
                        fmt='vol:{}',
                        # padding = 5,
                        volume_app='pactl',
                        volume_down_command='pactl set-sink-volume @DEFAULT_SINK@ -5%',
                        volume_up_command='pactl set-sink-volume @DEFAULT_SINK@ +5%',
                        decorations = [
                            RectDecoration(
                                colour = mocha["classicRose"],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                        ],
                        padding = 10,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5,
                ),
                widget.Clock(    
                    foreground = colors[0],
                    background = colors[0],
                    font = "CaskaydiaCove Nerd Font",
                    fontsize = 14,
                    format = "  %a, %d %b [ %I:%M %p ]",
                    timezone = 'Asia/Kolkata',
                    update_interval = 5,
                    decorations = [
                            RectDecoration(
                                colour = colors[8],
                                radius = 10, 
                                filled = True, 
                                # padding_y = 4,
                                padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 10,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5,
                ),
                widget.QuickExit(
                    background = colors[0],
                    # countdown_format = '  {} secs',
                    countdown_format = '{}',
                    countdown_start = 5,
                    # default_text = '襤 shutdown',
                    default_text = '',
                    fmt = '{}',
                    font = "CaskaydiaCove Nerd Font",
                    foreground = colors[0],
                    # width = 115,
                    width = 30,
                    timer_interval = 1,
                    decorations = [
                            RectDecoration(
                                colour = colors[6],
                                # radius = 10,
                                radius = 16,
                                filled = True,
                                padding_y = 4,
                                # padding_y = 7,
                                group = True
                            )
                    ],
                    padding = 10,
                ),
                widget.Spacer(
                    background = colors[0],
                    length = 5,
                ),
            ],
            # 30,
            size = 37,
            # margin = [5,5,5,5],
            # margin = [5,5,0,5],
            #margin = [7,7,0,7], # [N,E,S,W] or [top,right,bottom,left]
            margin = [7,10,0,10], # [N,E,S,W] or [top,right,bottom,left]
            opacity = 0.9, # range from 0 to 1, 0 is transparent & 1 is opaque, can use values like 0.2 or 0.8, etc
        )
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="mpv"),  # mpv
        Match(wm_class="Protonvpn"),  # Protonvpn
        Match(wm_class="GParted"),  # GParted
        Match(wm_class="Grub-customizer"),  # Grub-customizer
        Match(wm_class="Lxappearance"),  # Lxappearance
        #Match(wm_class="Bitwarden"),  # Bitwarden
        Match(wm_class="feh"),  # feh
        Match(wm_class="stacer"),  # stacer
        Match(wm_class="blurble"),  # blurble
        Match(wm_class="dosbox-staging"),  # dosbox-staging
        Match(wm_class="Galculator"),  # Galculator
        Match(wm_class="yad"),  # Yad
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

