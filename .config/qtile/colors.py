# Define atleast 9 colors in a colorscheme
#
# lushPlum
def lushPlum():
    # colors = [["#282A36", "#282A36"], # Background (Gun Metal) [0]
              # ["#E1ACFF", "#E1ACFF"], # Window Name, (Mauve) [2]
              # ["#a9a1e1", "#a9a1e1"], # Background (Dark grey) [9]
    colors = [["#1D2330", "#1D2330"], # Background, (Gun Metal) [0]
              ["#590696", "#590696"], # Left module base, (Metallic Violet) [1]
              ["#C6A0F6", "#C6A0F6"], # Window Name, (Pale Violet) [2]
              ["#8BE9FD", "#8BE9FD"], # Battery bg, (Cyan) [3]
              ["#7DC4E4", "#7DC4E4"], # Current Screen border-GroupBox, (Aero) [4]
              ["#9932CC", "#9932CC"], # GroupBox bg, (Dark Orchid) [5]
              ["#E94435", "#E94435"], # QuickExit bg, (Cinnabar) [6]
              ["#50FA7B", "#50FA7B"], # CheckUpdates bg, (Green) [7]
              ["#FFBF00", "#FFBF00"], # Clock bg, (Amber) [8]
    ]
    return colors

# dracula
def dracula():
    colors = [["#282c34", "#282c34"],
              ["#c678dd", "#c678dd"],
              ["#a9a1e1", "#a9a1e1"],
              ["#51afef", "#51afef"],
              ["#46d9ff", "#46d9ff"],
              ["#1c1f24", "#1c1f24"],
              ["#ff6c6b", "#ff6c6b"],
              ["#98be65", "#98be65"],
              ["#da8548", "#da8548"],
              ["#dfdfdf", "#dfdfdf"],
    ]
    return colors

# eveForest
def eveForest():
    colors = [
        ["#404B50", "#404B50"], # 
        ["#D698B6", "#D698B6"], # 
        ["#84C091", "#84C091"], #
        ["#7FBBB4", "#7FBBB4"], # 
        ["#D3C6A9", "#D3C6A9"], #
        ["#D3C6A9", "#D3C6A9"], #
        ["#E67E7F", "#E67E7F"], # 
        ["#A7C180", "#A7C180"], #
        ["#DBBD7F", "#DBBD7F"], # 
        ["#D3C6A9", "#D3C6A9"], #
    ]
    return colors

# def eveForest():
#     colors = [
#         ["#2B3339", "#2B3339"],
#         ["#E69875", "#E69875"],
#         ["#6272A4", "#6272A4"],
#         ["#D196B3", "#D196B3"],
#         ["#7C8377", "#7C8377"],
#         ["#7FBBB3", "#7FBBB3"],
#         ["#ED8082", "#ED8082"],
#         ["#A7C080", "#A7C080"],
#         ["#A7C080", "#A7C080"],
#         ["#D5C9AB", "#D5C9AB"],
#     ]
#     return colors

# nordFox
def nordFox():
    colors = [
        ["#2e3440", "#2e3440"], # Background () [0]
        ["#4b5668", "#4b5668"], # fg_gutter () [1]
        ["#c9826b", "#c9826b"], # Orange [2]
        ["#5fbad6", "#5fbad6"], # Magenta [3]
        ["#81a1c1", "#81a1c1"], # Blue [4]
        ["#d0aeae", "#d0aeae"], # White [5]
        ["#bf616a", "#bf616a"], # red [6]
        ["#a3be8c", "#a3be8c"], # Green [7]
        ["#ebcb8b", "#ebcb8b"], # Yellow [8]
        ["#bf88bc", "#bf88bc"], # Pink [9]
    ]
    return colors

# oneDark
def oneDark():
    colors = [
        ["#282C34", "#282C34"],
        ["#E06C75", "#E06C75"],
        ["#98C379", "#98C379"],
        ["#E5C07B", "#E5C07B"],
        ["#61AFEF", "#61AFEF"],
        ["#C678DD", "#C678DD"],
        ["#56B6C2", "#56B6C2"],
        ["#ABB2BF", "#ABB2BF"],
    ]
    return colors

