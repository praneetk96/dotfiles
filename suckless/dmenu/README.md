# What is dmenu?
dmenu is a dynamic menu for X, originally designed for dwm. It manages large numbers of user-defined menu items efficiently.

## Screenshot
![dotfiles-dwm](https://gitlab.com/praneetk96/dotfiles/-/raw/main/screenshots/dotfiles-dmenu.png)

## Description
This is my version of dmenu config, in which I configured font, I like to use dracula color scheme and finally did some patching which are as follows:
- border
- center 

## TODO
Have to be patched eventually:
- highlight
- lineheight
- morecolor

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.
