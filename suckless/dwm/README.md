# What is dwm?
dwm is a dynamic window manager for X. It manages windows in tiled, monocle and floating layouts. All of the layouts can be applied dynamically, optimising the environment for the application in use and the task performed.

## Screenshot
![dotfiles-dwm](https://gitlab.com/praneetk96/dotfiles/-/raw/main/screenshots/dotfiles-dwm.png)

## Description
This is my version of dwm config, in which I configured font, I like to use dracula color scheme and finally did some patching which are as follows:
- alwayscenter
- autostart
- uselessgap

## TODO
Have to be patched eventually:
- alpha
- attachbelow

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.
