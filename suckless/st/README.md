# What is st?
st is a simple terminal implementation for X.

## Screenshot
![dotfiles-dwm](https://gitlab.com/praneetk96/dotfiles/-/raw/main/screenshots/dotfiles-st.png)

## Description
This is my version of st config, in which I configured font & did some patching which are as follows:
- alpha
- dracula

## TODO
Things I'll patch in future will be:
- scrollback
- w3m 

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.
