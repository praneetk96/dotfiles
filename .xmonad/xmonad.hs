-- Base
import XMonad
import System.Directory
import System.IO (hClose, hPutStr, hPutStrLn)
import System.Exit
import qualified XMonad.StackSet as W
-- import System.Exit (exitSuccess)

-- Actions
import XMonad.Actions.CopyWindow (kill1, copy)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S
-- import XMonad.Actions.Volume

-- Control
import Control.Monad

-- Data
import Data.Char (isSpace, toUpper)
import Data.List -- for grouping the classNames in ManageHook
import Data.Monoid
import Data.Maybe (fromJust, isJust)
import Data.Tree
import qualified Data.Map as M
-- import Data.Maybe (fromJust)

-- Graphics
import Graphics.X11.ExtraTypes.XF86

-- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.WorkspaceHistory

-- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile (MirrorResize (MirrorExpand, MirrorShrink), ResizableTall (ResizableTall))
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

-- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
-- import XMonad.Layout.Renamed (renamed, Rename(Replace, CutWordsLeft))
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

-- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP, mkNamedKeymap, additionalKeysP, additionalMouseBindings)
import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
-- import XMonad.Util.Hacks (windowedFullscreenFixEventHook, javaHack, trayerAboveXmobarEventHook, trayAbovePanelEventHook, trayerPaddingXmobarEventHook, trayPaddingXmobarEventHook, trayPaddingEventHook)

   -- ColorScheme module (SET ONLY ONE!)
      -- Possible choice are:
      -- DoomOne
      -- Dracula
      -- GruvboxDark
      -- MonokaiPro
      -- Nord
      -- OceanicNext
      -- Palenight
      -- SolarizedDark
      -- SolarizedLight
      -- TomorrowNight
--    -- LushPlum
import Colors.Dracula

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
-- myTerminal      = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
-- myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--

-- myFont :: String
-- myFont = "xft:CaskaydiaCove Nerd Font Mono:regular:size=9:antialias=true:hinting=true"
myFont = "xft:Ubuntu Nerd Font:regular:size=9:antialias=true:hinting=true"

myTerminal :: String
-- myTerminal = "alacritty"
myTerminal = "xfce4-terminal"

myAltTerminal :: String
-- myAltTerminal = "alacritty"
myAltTerminal = "xfce4-terminal"
-- myAltTerminal = "urxvt"

myModMask :: KeyMask
myModMask = mod4Mask

myBrowser :: String
myBrowser = "firefox"     -- Sets firefox as browser
-- myBrowser = "brave-browser"     -- Sets brave as browser

myPrivateBrowser :: String
myPrivateBrowser = "firefox --private-window"
-- myPrivateBrowser = "brave-browser --incognito"

myAltBrowser :: String
myAltBrowser = "qutebrowser"     -- Sets qutebrowser as alternate browser 

myFileManager :: String
myFileManager = "thunar"  -- Sets thunar as file manager

myCalculator :: String
myCalculator = "galculator"

myScreenShooter :: String
myScreenShooter = "xfce4-screenshooter"

myDmenu :: String
-- myDmenu = "dmenu_run -b -nf '#560696' -nb '#C6A0F6' -sb '#590696' -sf '#C6A0F6' -fn 'caskaydiaCove-14' -p 'Search: '"
myDmenu = "dmenu_run -b -nf '#560696' -nb '#C6A0F6' -sb '#590696' -sf '#C6A0F6' -fn 'UbuntuNerdFont-14' -p 'Search: '"

myPatchedDmenu :: String
myPatchedDmenu = "dmenu_run -c -l 20 -nf '#560696' -nb '#C6A0F6' -sb '#590696' -sf '#C6A0F6' -fn 'JetBrains Mono:size14' -p 'Run: '"

myScreenLocker :: String
myScreenLocker = "betterlockscreen -l dimblur"
-- myScreenLocker = "i3lock --show-failed-attempts --image /usr/share/backgrounds/lockScreenWall.png"

myEditor :: String
myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor

myBorderWidth :: Dimension
myBorderWidth = 2      -- Sets border width for windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

toggleFloat :: Window -> X ()
toggleFloat w =
  windows
    ( \s ->
        if M.member w (W.floating s)
          then W.sink w s
          else (W.float w (W.RationalRect (1 / 3) (1 / 4) (1 / 2) (1 / 2)) s)
    )

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_k)     , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         ,((0,xK_space) , setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                (0x28,0x2c,0x34) -- lowest inactive bg
                (0x28,0x2c,0x34) -- highest inactive bg
                (0xc7,0x92,0xea) -- active bg
                (0xc0,0xa7,0x9a) -- inactive fg
                (0x28,0x2c,0x34) -- active fg

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
-- myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]
-- myWorkspaces    = ["I","II","III","IV","V","VI","VII","VIII","IX"]
-- myWorkspaces = [" 一 ", " 二 ", " 三 ", " 四 ", " 五 ", " 六 ", " 七 ", " 八 ", " 九 "]
myWorkspaces = [" www ", " dev ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]

myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#1D2330"
myFocusedBorderColor = "#E1ACFF"
-- myNormalBorderColor  = "#060D12"
-- myFocusedBorderColor = "#9fc7d2"

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
-- START_KEYS
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    -- KB_GROUP Xmonad
    [ ((modm .|. shiftMask , xK_r ), spawn "xmonad --recompile; xmonad --restart")
    , ((modm .|. shiftMask , xK_q ), io (exitWith ExitSuccess))
    , ((modm , xK_Escape ), spawn "./.config/scripts/xmonad-logout.sh")
    , ((modm , xK_F1 ), spawn "./.xmonad/xmonad_keys.sh")
    -- , ((modm .|. shiftMask , xK_c ), kill1)
    , ((modm , xK_w ), kill1)
    , ((modm .|. shiftMask , xK_a ), killAll)

    -- KB_GROUP Applications
    -- , ((modm .|. shiftMask , xK_Return ), spawn "rofi -show drun")
    , ((modm .|. shiftMask , xK_Return ), spawn "xfce4-appfinder")
    , ((modm .|. shiftMask , xK_b ), sendMessage ToggleStruts)
    -- , ((modm , xK_Return ), spawn (myTerminal))
    -- , ((modm , xK_Return ), spawn myTerminal)
    , ((modm , xK_Return ), spawn (myTerminal ++ " -e /bin/fish"))
    , ((controlMask , xK_Return ), spawn myAltTerminal)
    , ((modm .|. controlMask , xK_h ), spawn (myTerminal ++ " -e htop"))
    , ((modm .|. controlMask , xK_m ), spawn (myTerminal ++ "xdotool key --clearmodifiers 'super+Tab'") >> spawn (myTerminal ++ " -e mocp") >> spawn (myTerminal ++ " -e cava"))
    -- , ((modm .|. shiftMask , xK_x ), spawn "killall trayer && killall xmobar")    -- for debug
    , ((modm .|. shiftMask , xK_x ), spawn "killall stalonetray && killall xmobar")    -- for debug

    -- launch browser
    , ((modm , xK_b ), spawn myBrowser)
    , ((modm .|. controlMask , xK_b ), spawn myPrivateBrowser)
    , ((mod1Mask , xK_b ), spawn myAltBrowser)

    -- launch file manager
    , ((modm , xK_f ), spawn myFileManager)

    -- launch dmenu
    -- , ((modm , xK_p ), spawn myDmenu)
    , ((modm , xK_p ), spawn myPatchedDmenu)

    -- eject cd rom
    , ((controlMask .|. shiftMask, xK_e ), spawn "eject /dev/cdrom")
    
    -- KB_GROUP Monitors
    , ((modm , xK_period ), nextScreen)
    , ((modm , xK_comma ), prevScreen)

    -- KB_GROUP Layouts
    -- Switch layouts
    , ((modm , xK_Tab ), sendMessage NextLayout)
    , ((modm , xK_space ), sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)
    
    -- Push window back into tiling
    , ((modm, xK_t ), withFocused $ windows . W.sink)
    
    -- Window resizing
    , ((modm , xK_h ), sendMessage Shrink)
    , ((modm , xK_l ), sendMessage Expand)
    -- , ((modm .|. mod1Mask , xK_j ), sendMessage MirrorShrink)
    -- , ((modm .|. mod1Mask , xK_k ), sendMessage MirrorExpand)
    , ((modm .|. shiftMask , xK_j ), sendMessage MirrorShrink)
    , ((modm .|. shiftMask , xK_k ), sendMessage MirrorExpand)

    -- Increase/decrease spacing (gaps)
    , ((controlMask .|. mod1Mask, xK_j ), decWindowSpacing 4)
    , ((controlMask .|. mod1Mask, xK_k ), incWindowSpacing 4)
    , ((controlMask .|. mod1Mask, xK_h ), decScreenSpacing 4)
    , ((controlMask .|. mod1Mask, xK_l ), incScreenSpacing 4)
    
    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    , ((modm .|. controlMask, xK_space ), withFocused toggleFloat)

    -- Resize viewed windows to the correct size
    , ((modm, xK_n ), refresh)

    -- KB_GROUP Window Navigation
    , ((modm, xK_Right ), windows W.focusDown)
    , ((modm, xK_Left ), windows W.focusUp)
    , ((modm, xK_m ), windows W.focusMaster)
    , ((modm .|. shiftMask, xK_Right ), windows W.swapDown)
    , ((modm .|. shiftMask, xK_Left ), windows W.swapUp)
    , ((modm .|. shiftMask, xK_m ), windows W.swapMaster)
   
    -- Increase/decrease windows in the master pane or the stack
    -- Increment the number of windows in the master area
    , ((modm .|. shiftMask, xK_Up ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm .|. shiftMask, xK_Down ), sendMessage (IncMasterN (-1)))

    -- Increase max # of windows for layout
    , ((modm , xK_equal ), increaseLimit)
    
    -- Decrease max # of windows for layout
    , ((modm , xK_minus ), decreaseLimit)

    -- KB_GROUP Multimedia keys
    -- control brightness
    , ((0, xF86XK_MonBrightnessUp), spawn "brightnessctl set +5% -e 3")
    , ((0, xF86XK_MonBrightnessDown), spawn "brightnessctl set 5%- -e 3")

    -- control volume
    , ((0, xF86XK_AudioMute ), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ((0, xF86XK_AudioLowerVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
    , ((0, xF86XK_AudioRaiseVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")

    -- -- multimedia keys, I have set it to control deadbeef music player (lightweight)
    -- , ((0, xF86XK_AudioPlay ), spawn "deadbeef --play-pause")
    -- , ((0, xF86XK_AudioPause ), spawn "deadbeef --play-pause")
    -- , ((0, xF86XK_AudioStop ), spawn "deadbeef --stop")
    -- , ((0, xF86XK_AudioPrev ), spawn "deadbeef --prev")
    -- , ((0, xF86XK_AudioNext ), spawn "deadbeef --next")

    -- -- multimedia keys, I have set it to control audacious music player (has equalizer, better audio output)
    , ((0, xF86XK_AudioPlay ), spawn "audacious --play-pause")
    , ((0, xF86XK_AudioPause ), spawn "audacious --play-pause")
    , ((0, xF86XK_AudioStop ), spawn "audacious --stop")
    , ((0, xF86XK_AudioPrev ), spawn "audacious --rew")
    , ((0, xF86XK_AudioNext ), spawn "audacious --fwd")

    -- other keys
    , ((0, xF86XK_HomePage ), spawn (myBrowser ++ " https://www.codechef.com"))
    , ((0, xF86XK_Calculator ), spawn myCalculator)
    , ((0, xF86XK_PowerOff ), spawn "systemctl poweroff")
    , ((0, xF86XK_Sleep ), spawn "systemctl suspend")
    , ((0, xF86XK_WakeUp ), spawn "systemctl wake")
    , ((0, xF86XK_ScreenSaver), spawn myScreenLocker)
    , ((0, xK_Print ), spawn myScreenShooter)
    
    --  Reset the layouts on the current workspace to default
    -- , ((modm .|. shiftMask,    xK_space    ), setLayout $ XMonad.layoutHook conf)

    -- , ((modm .|. controlMask,  xK_space    ), withFocused toggleFloat)

    -- Resize viewed windows to the correct size
    -- , ((modm,                  xK_n        ), refresh)

    -- Window Navigation
    -- , ((modm,                  xK_Right    ), windows W.focusDown)
    -- , ((modm,                  xK_Left     ), windows W.focusUp)
    -- , ((modm,                  xK_m        ), windows W.focusMaster)
    -- , ((modm .|. shiftMask,    xK_Right    ), windows W.swapDown)
    -- , ((modm .|. shiftMask,    xK_Left     ), windows W.swapUp)
    -- , ((modm .|. shiftMask,    xK_m        ), windows W.swapMaster)
   
    -- Increase/decrease windows in the master pane or the stack
    -- Increment the number of windows in the master area
    -- , ((modm .|. shiftMask,    xK_Up   ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    -- , ((modm .|. shiftMask,    xK_Down ), sendMessage (IncMasterN (-1)))

    -- Increase max # of windows for layout
    -- , ((modm                   ,  xK_equal ), increaseLimit)
    
    -- Decrease max # of windows for layout
    -- , ((modm                   ,  xK_minus ), decreaseLimit)

    -- -- Move window to WS and go there
    -- , ((modm .|. shiftMask,    xK_Page_Up   ), shiftTo Next nonNSP >> moveTo Next nonNSP)
    -- , ((modm .|. shiftMask,    xK_Page_Down ), shiftTo Prev nonNSP >> moveTo Prev nonNSP)

    -- , ((modm,               xK_Tab   ), windows W.focusDown)

    -- -- Move focus to the next window
    -- , ((modm,               xK_j     ), windows W.focusDown)

    -- -- Move focus to the previous window
    -- , ((modm,               xK_k     ), windows W.focusUp  )

    -- -- Move focus to the master window
    -- , ((modm,               xK_m     ), windows W.focusMaster  )

    -- -- Swap the focused window and the master window
    -- , ((modm,               xK_Return), windows W.swapMaster)

    -- -- Swap the focused window with the next window
    -- , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- -- Swap the focused window with the previous window
    -- , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- -- Shrink the master area
    -- , ((modm,               xK_h     ), sendMessage Shrink)

    -- -- Expand the master area
    -- , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    -- , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- -- Increment the number of windows in the master area
    -- , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- -- Deincrement the number of windows in the master area
    -- , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- -- Quit xmonad
    -- , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- -- Restart xmonad
    -- , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    -- , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]
    -- END_KEYS
    -- ++

    -- --
    -- -- mod-[1..9], Switch to workspace N
    -- -- mod-shift-[1..9], Move client to workspace N
    -- --
    
    -- [((m .|. modm, k), windows $ f i)
    --     | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
    --     , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

      ++ moveWindow
      ++ changeWS
    where
        moveWindow =
            [ ((m .|. modm, k), windows $ f i)
              | (i, k) <- zip (XMonad.workspaces conf) ([xK_1 .. xK_9] ++ [xK_0]),
                (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
            ]
        changeWS =
            [ ((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
              | (key, sc) <- zip [xK_s, xK_a] [0 ..],
                (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
            ]

    -- ---I DISABLED THIS AS I DON'T HAVE MULTI MONITOR SETUP
    -- ++

    -- --
    -- -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    -- --
    --
    -- [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
    --     | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
    --     , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
-- mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True
mySpacing i = spacingRaw False (Border 0 i 0 i) True (Border i 0 i 0) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
-- mySingleSpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
-- mySingleSpacing i = spacingRaw True (Border i i i i) True (Border i i i i) True
mySingleSpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySingleSpacing i = spacingRaw True (Border 0 i 0 i) True (Border i 0 i 0) True

-- spacingRaw is no longer used. And mySpacing, mySpacing' is of no longer use as smartSpacing do the same thing
-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--

-- myLayout = renamed [CutWordsLeft 1] $  avoidStruts $ smartBorders $ mySingleSpacing 10 (tiled ||| Mirror tiled ||| Full)
--   where
--      -- default tiling algorithm partitions the screen into two panes
--      tiled   = Tall nmaster delta ratio

--      -- The default number of windows in the master pane
--      nmaster = 1

--      -- Default proportion of screen occupied by master pane
--      ratio   = 1/2

--      -- Percent of screen to increment by when resizing panes
--      delta   = 3/100

myLayout = renamed [CutWordsLeft 1] $  avoidStruts $ smartBorders (
  mySpacing 10 (Tall 1 (3/100) (1/2)) ||| 
  mySpacing 10 (Mirror (Tall 1 (3/100) (1/2))) ||| 
  mySingleSpacing 10 (Full))

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
  { swn_font              = "xft:Ubuntu:bold:size=60"
  , swn_fade              = 1.0
  , swn_bgcolor           = "#1c1f24"
  , swn_color             = "#ffffff"
  }

-- ----------------------------------------------------------
-- tall = renamed [Replace "tall"]
--        $ limitWindows 5
--        $ smartBorders
--        $ WindowNavigation
--        $ sublayout [] (smartBorders Simplest)
--        $ mySpacing 
-- full = renamed [Replace "full"]
--        $ mySingleSpacing

-- -- setting colors for tabs layout and tabs sublayout.
-- myTabTheme = def { fontName            = myFont
--                  , activeColor         = color15
--                  , inactiveColor       = color08
--                  , activeBorderColor   = color15
--                  , inactiveBorderColor = colorBack
--                  , activeTextColor     = colorBack
--                  , inactiveTextColor   = color16
--                  }

-- -- layout hook
-- myLayout = avoidStruts
--           $ mouseResize
--           $ windowArrange
--           -- $ T.toggleLayouts floats
--           $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout

--   where
--     myDefaultLayout = withBorder myBorderWidth tall
--                                            ||| full
-- ----------------------------------------------------------

------------------------------------------------------------------------
-- classNames of frequently used applications
browsers = ["firefox", "Transmission-gtk", "librewolf-default", "brave-browser", "qutebrowser"]
editor = ["VSCodium", "Code", "code-oss", "lapce", "Geany", "jetbrains-pycharm-ce"]
system = ["GParted", "Grub-customizer", "Lxappearance", "Bitwarden"]
docs = ["libreoffice", "libreoffice-startcenter"]
games = ["VirtualBox Manager", "dosbox-staging", "heroic", "Steam", "RiseupVPN"]
messages = ["Thunderbird", "thunderbird","TelegramDesktop","Signal", "Gfeeds"]
music = ["Audacious", "Deadbeef", "Tauon Music Box", "Audacity", "Lollypop", "Harmonoid", "com.github.taiko2k.tauonmb"]
video = ["Parole", "vlc", "obs", "SimpleScreenRecorder"]
gfx = ["Inkscape", "Gimp-2.10", "kdenlive"]

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
-- myManageHook = composeAll
--     [ className =? "MPlayer"        --> doFloat
--     , className =? "Gimp"           --> doFloat
--     , className =? "mpv"            --> doFloat
--     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
--     , className =? "Galculator"     --> doCenterFloat
--     , resource  =? "desktop_window" --> doIgnore
--     , resource  =? "kdesktop"       --> doIgnore 
--     , className =? "Yad"            --> doCenterFloat

--     -- , [ className =? c --> viewShift (myWorkspaces !! 0) | c <- browsers ]
--     -- , className =? "firefox"        --> viewShift (myWorkspaces !! 0)
--     -- , className =? "Transmission-gtk"        --> viewShift (myWorkspaces !! 0)
--     -- , className =? "VSCodium"        --> viewShift (myWorkspaces !! 1)
--     -- , className =? "Code"        --> viewShift (myWorkspaces !! 1)
--     -- , className =? "lapce"        --> viewShift (myWorkspaces !! 1)
--     -- , className =? "GParted"        --> viewShift (myWorkspaces !! 2)
--     -- , className =? "Grub-customizer"        --> viewShift (myWorkspaces !! 2)
--     -- , className =? "Lxappearance"        --> viewShift (myWorkspaces !! 2)
--     -- , className =? "libreoffice"        --> viewShift (myWorkspaces !! 3)
--     -- , className =? "VirtualBox Manager"        --> viewShift (myWorkspaces !! 4)
--     -- , className =? "dosbox-staging"        --> viewShift (myWorkspaces !! 4)
--     -- , className =? "heroic"        --> viewShift (myWorkspaces !! 4)
--     -- , className =? "TelegramDesktop"            --> viewShift ( myWorkspaces !! 5 )
--     -- , className =? "Gimp"           --> doShift ( myWorkspaces !! 8 )
--     -- , className =? "Deadbeef"           --> doShift ( myWorkspaces !! 6 )
--     , className =? "Deadbeef"           --> viewShift ( myWorkspaces !! 6 )
--     ]
--     where viewShift = doF . liftM2 (.) W.greedyView W.shift

myManageHook = 
    composeAll . concat $
    [     
          [className =? "mpv"                                 --> doFloat]
        , [className =? "Gimp"                                --> doFloat]
        , [(className =? "firefox" <&&> resource =? "Dialog") --> doFloat]
        , [className =? "Galculator"                          --> doCenterFloat]
        , [resource  =? "desktop_window"                      --> doIgnore]
        , [resource  =? "kdesktop"                            --> doIgnore ]
        , [className =? "Xfce4-appfinder"                     --> doCenterFloat]
        , [className =? "URxvt"                               --> doCenterFloat]
        , [className =? "Yad"                                 --> doCenterFloat]
        , [className =? "GParted"                             --> doCenterFloat]
        , [className =? "Lxappearance"                        --> doCenterFloat]
        , [className =? "Grub-customizer"                     --> doCenterFloat]
        , [className =? "RiseupVPN"                           --> doCenterFloat]
        , [isDialog                                           --> doCenterFloat]
        , [isFullscreen                                       --> doCenterFloat]
        , [className =? c --> viewShift (myWorkspaces !! 0) | c <- browsers]
        , [className =? c --> viewShift (myWorkspaces !! 1) | c <- editor]
        , [className =? c --> viewShift (myWorkspaces !! 2) | c <- system]
        , [className =? c --> viewShift (myWorkspaces !! 3) | c <- docs]
        , [className =? c --> viewShift (myWorkspaces !! 4) | c <- games]
        , [className =? c --> viewShift (myWorkspaces !! 5) | c <- messages]
        , [className =? c --> viewShift (myWorkspaces !! 6) | c <- music]
        , [className =? c --> viewShift (myWorkspaces !! 7) | c <- video]
        , [className =? c --> viewShift (myWorkspaces !! 8) | c <- gfx]
    ]
    where viewShift = doF . liftM2 (.) W.greedyView W.shift

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
        -- ENABLED --
        spawnOnce "dunst &"
        spawnOnce "pasystray &"
        spawnOnce "numlockx on &"
        spawnOnce "redshift-gtk &"
        spawnOnce "lxsession -n -a &"
        spawnOnce "/usr/bin/lxpolkit &"
        spawnOnce "/usr/bin/emacs --daemon &"
        -- spawnOnce "betterlockscreen -u ~/Pictures/WallHaven/ &"
        spawnOnce "~/.xmonad/countDnfUpdates.sh &"
        spawn "sleep 2 && stalonetray --dockapp-mode simple --parent-bg false -geometry 3x1-0+0 --max-geometry 3x1-900+0 --background '#1D2330' --grow-gravity E --icon-gravity E --icon-size 24 --transparent false --window-strut none --window-type dock --window-layer top --no-shrink true --skip-taskbar true --slot-size 4 --scrollbars horizontal --scrollbars-highlight red --scrollbars-step 8 --scrollbars-size 4"
        spawnOnce "picom --unredir-if-possible --backend=glx --no-vsync --blur-method dual_kawase --blur-strength=3 --no-use-damage --config /home/gideon/.config/picom/xmonad-picom.conf &"
        spawnOnce "nitrogen --restore &"
        spawnOnce "nm-applet &"
        spawnOnce "xsetroot -cursor_name left_ptr -cursor_name Bibata-Modern-Ice &"
        spawnOnce "thunar --daemon &"
        spawnOnce "xfce4-power-manager &"

        -- DISABLED --
        --spawnOnce "feh -z --bg-fill --no-fehbg ~/Downloads/.gallery-dl/wallhaven/Alx/ &"
        --spawnOnce "~/.local/bin/dnfupdate &"
        --spawnOnce "feh -z --bg-fill --no-fehbg ~/Pictures/WallHaven &"
        --spawn "killall stalonetray"  -- kill current stalonetray on each restart
        --spawn "sleep 3 && killall dnf"
        --spawnOnce "picom --unredir-if-possible --backend=glx --vsync --blur-method dual_kawase --blur-strength=3 &"
        -- spawnOnce "feh --bg-fill --no-fehbg ~/Downloads/.gallery-dl/wallhaven/Alx/wallhaven_85g1v1_3136x1792.png &"
        -- spawnOnce "xfce4-session &"
        -- spawnOnce "/usr/bin/bash ~/.config/scripts/autoWall.sh" -- This script sets the wallpaper according to time of the day using nitrogen
        -- spawnOnce "nitrogen --restore &"
        -- spawn "taffybar &"
        --spawnOnce "/usr/local/bin/betterlockscreen -u ~/Downloads/.gallery-dl/wallhaven/Alx/wallhaven_m3327m_3641x2048.png &"
        -- spawn "killall trayer"  -- kill current trayer on each restart
        -- spawn("sleep 2 && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 22 --iconspacing 8")
        -- spawn("sleep 2 && stalonetray --dockapp-mode simple --parent-bg false -geometry 5x1-295+0 --max-geometry 8x1-900+0 --background '#1D2330' --grow-gravity E --icon-gravity E --icon-size 24 --transparent false --window-strut none --window-type dock --window-layer top --no-shrink true --skip-taskbar true")
        -- spawn("sleep 2 && stalonetray --dockapp-mode simple --parent-bg false -geometry 1x1-295+0 --max-geometry 8x1-900+0 --background '#1D2330' --grow-gravity E --icon-gravity E --icon-size 24 --transparent false --window-strut none --window-type dock --window-layer top --no-shrink true --skip-taskbar true")
        -- spawn("sleep 2 && stalonetray --dockapp-mode simple --parent-bg false -geometry 3x1-295+0 --max-geometry 3x1-900+0 --background '#1D2330' --grow-gravity E --icon-gravity E --icon-size 24 --transparent false --window-strut none --window-type dock --window-layer top --no-shrink true --skip-taskbar true --slot-size 4 --scrollbars horizontal --scrollbars-highlight red --scrollbars-step 8 --scrollbars-size 4")
        -- spawn("sleep 2 && stalonetray --dockapp-mode simple --parent-bg false -geometry 5x1-470+0 --max-geometry 8x1-900+0 --background '#1D2330' --grow-gravity E --icon-gravity E --icon-size 24 --transparent false --window-strut none --window-type dock --window-layer top --no-shrink true --skip-taskbar true")
        -- spawnOnce "/usr/local/bin/betterlockscreen -u Pictures/WallHaven/wallhaven-g78omd.png &"
        -- spawn("killall trayer; sleep 2 && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 22 --iconspacing 8")
        -- spawnOnce "blueberry-tray &"
        -- spawnOnce "xfce4-clipman &"
        -- spawn "killall volumeicon"
        -- spawn "killall stalonetrayrc"
        -- spawnOnce "dunst &"
        -- spawnOnce "xfce4-volumed-pulse &"
        -- spawn "killall stalonetray"  -- kill current stalonetray on each restart
        -- spawn ("sleep 2 && stalonetray -c .config/stalonetrayrc --sticky &")
        -- spawnOnce "udiskie &"
        -- spawn "killall udiskie"
        -- spawn "killall redshift-gtk"
        -- spawn "killall redshift-gtk"
        -- spawn ("sleep 2 && trayer --edge top --align right --SetDockType true --SetPartialStrut true  --expand true --width 7 --transparent false --height 21 --padding 4 --iconspacing 4")
        -- spawn ("sleep 2 && trayer --edge top --align right --SetDockType true --SetPartialStrut true  --expand true --width 6 --transparent false --height 21 --padding 4 --iconspacing 8")
        -- spawn ("sleep 2 && trayer --edge top --align right --SetDockType true --SetPartialStrut true  --expand true --width 6 --transparent false --height 21 --padding 4 --iconspacing 8 --tint '#1D2330'")
        -- spawn ("sleep 2 && trayer --edge top --align right --SetDockType true --SetPartialStrut true  --expand true --width 5 --transparent false --tint 0x5f5f5f --height 21 --padding 3 --iconspacing 4")
        -- spawn("sleep 2 && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 " ++ colorTrayer ++ " --height 22 --iconspacing 8 --margin 455")

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
-- main = xmonad defaults
-- main = xmonad $ docks defaults
main = do
  xmproc <- spawnPipe "/usr/bin/xmobar /home/gideon/.config/xmobar/xmobarrc"
  -- xmproc <- spawnPipe "/usr/bin/xmobar /home/gideon/.config/xmobar/xmobar.hs"
  xmonad $ docks defaults {
    logHook = dynamicLogWithPP $ xmobarPP
                        --{ ppOutput = hPutStrLn xmproc,
                        --  ppCurrent = xmobarColor "#98be65" "" . wrap "[" "]",
                        --  ppVisible = xmobarColor "#98be65" "",
                        --  ppHidden = xmobarColor "#82AAFF" "" . wrap "*" "",
                        --  ppHiddenNoWindows = xmobarColor "#c792ea" "",
                        --  ppTitle = xmobarColor "#b3afc2" "" . shorten 60,
                        --  ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>",
                        --  ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!",
                        --  ppExtras  = [windowCount],
                        --  ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        --}
                        { ppOutput = hPutStrLn xmproc
                        -- , ppCurrent = xmobarColor color06 "" . wrap "[" "]"
                        , ppCurrent = xmobarColor color06 "" . wrap
                                     ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
                        , ppVisible = xmobarColor color06 "" . clickable
                        -- , ppHidden = xmobarColor color05 "" . wrap "*" ""
                        , ppHidden = xmobarColor color05 "" . wrap
                                    ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>" . clickable
                        , ppHiddenNoWindows = xmobarColor color05 "" . clickable
                        , ppTitle = xmobarColor color16 "" . shorten 60
                        -- , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"
                        , ppSep =  "<fc=" ++ color09 ++ "> <fn=0>|</fn> </fc>"
                        , ppUrgent = xmobarColor color02 "" . wrap "!" "!"
                        , ppExtras  = [windowCount]
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
  }

-- main = do
--   xmproc <- spawnPipe "/usr/bin/xmobar /home/gideon/.config/xmobar/xmobarrc"
--   --xmonad $ docks defaults
--   xmonad $ docks defaults {
--     logHook = dynamicLogWithPP $  filterOutWsPP $ xmobarPP
--         { ppOutput = \x -> hPutStrLn xmproc x   -- xmobar on monitor 1
--         , ppCurrent = xmobarColor color06 "" . wrap
--                       ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
--           -- Visible but not current workspace
--         , ppVisible = xmobarColor color06 "" . clickable
--           -- Hidden workspace
--         , ppHidden = xmobarColor color05 "" . wrap
--                      ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>" . clickable
--           -- Hidden workspaces (no windows)
--         , ppHiddenNoWindows = xmobarColor color05 ""  . clickable
--           -- Title of active window
--         , ppTitle = xmobarColor color16 "" . shorten 60
--           -- Separator character
--         , ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"
--           -- Urgent workspace
--         , ppUrgent = xmobarColor color02 "" . wrap "!" "!"
--           -- Adding # of windows on current workspace to the bar
--         , ppExtras  = [windowCount]
--           -- order of things in xmobar
--         , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
--         }
--   }

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        -- layoutHook         = myLayout,
        layoutHook         = showWName' myShowWNameTheme $ myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]


