# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# navigation
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}


### ALIASES ###
# Edit config files
alias edit-qtile='/usr/bin/vim .config/qtile/config.py'
alias pyLec="cd && cd Videos/Jenny\'s\ Lectures/Python\ Full\ Course/"
alias whichClass='xprop | grep WM_CLASS'
alias maskTlp='sudo systemctl mask power-profiles-daemon.service && sudo systemctl mask systemd-rfkill.service && sudo systemctl mask systemd-rfkill.socket'
alias unmaskTlp='sudo systemctl unmask power-profiles-daemon.service && sudo systemctl unmask systemd-rfkill.service && sudo systemctl unmask systemd-rfkill.socket'
alias editTlp='sudo vim /etc/tlp.d/01-myconfig.conf'
alias whichGpu='glxinfo | egrep "OpenGL vendor|OpenGL renderer"'
alias cmatrix='cmatrix -a -u 8 -C magenta'

alias makeIso='echo "sudo dd bs=4M if=path/to/archlinux-version-x86_64.iso of=/dev/sdx conv=fsync oflag=direct status=progress"'

# Changing "cat" to "bat"
alias cat='bat'

# crontab
alias listCron='sudo crontab -u gideon -l'
alias listCronRoot='sudo crontab -u root -l'
alias editCron='sudo crontab -u gideon -e'
alias editCronRoot='sudo crontab -u root -e'
alias rmCron='sudo crontab -r -u gideon'
alias rmCronRoot='sudo crontab -r -u root'

# Shutdown at specified time
alias shtd='sudo shutdown 5:57'

# Cancel autoShutdown
alias shtdc='shutdown -c'

# Display image in kitty
alias icat="kitty +kitten icat"

# Completion for kitty
source <(kitty + complete setup bash)

# User Aliases
alias sch='sudo find / -iname'

# Neovim
alias v='nvim'

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# dnf and flatpak
alias dnfupd='sudo dnf update'                                      # update only standard pkgs
alias dnfuupd='sudo dnf upgrade --refresh && sudo dnf update'       # Refresh pkglist & update standard pkgs
alias fpakupd='flatpak update'                                      # update only Flatpak pkgs
alias rmorp='sudo dnf autoremove'                                   # remove orphaned packages

# install/remove desktop environment
alias install-gnome='sudo dnf groupinstall gnome-games gnome-desktop critical-path-gnome gnome-software-development'
alias install-kde='sudo dnf groupinstall kde-desktop-environment kde-office kde-apps kde-education kde-media kde-telepathy kde-desktop'
alias install-xfce='sudo dnf groupinstall xfce-desktop-environment xfce-apps xfce-desktop xfce-extra-plugins xfce-media xfce-office'
alias install-lxde='sudo dnf groupinstall lxde-desktop-environment lxde-apps lxde-desktop lxde-media lxde-office'
alias install-lxqt='sudo dnf groupinstall lxqt-desktop-environment lxqt-apps lxqt-desktop lxqt-l10n lxqt-media lxqt-office'
alias install-cinnamon='sudo dnf groupinstall cinnamon-desktop-environment cinnamon-desktop'
alias install-deepin='sudo dnf groupinstall deepin-desktop-environment deepin-desktop-apps deepin-desktop-media deepin-desktop-office deepin-desktop'

alias rm-gnome='sudo dnf groupremove gnome-games gnome-desktop critical-path-gnome gnome-software-development'
alias rm-kde='sudo dnf groupremove kde-desktop-environment kde-office kde-apps kde-education kde-media kde-telepathy kde-desktop'
alias rm-xfce='sudo dnf groupremove xfce-desktop-environment xfce-apps xfce-desktop xfce-extra-plugins xfce-media xfce-office'
alias rm-lxde='sudo dnf groupremove lxde-desktop-environment lxde-apps lxde-desktop lxde-media lxde-office'
alias rm-lxqt='sudo dnf groupremove lxqt-desktop-environment lxqt-apps lxqt-desktop lxqt-l10n lxqt-media lxqt-office'
alias rm-cinnamon='sudo dnf groupremove cinnamon-desktop-environment cinnamon-desktop'
alias rm-deepin='sudo dnf groupremove deepin-desktop-environment deepin-desktop-apps deepin-desktop-media deepin-desktop-office deepin-desktop'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

# ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias psmem='ps auxf | sort -nr -k 4'
alias pscpu='ps auxf | sort -nr -k 3'

# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'

# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias tag='git tag'
alias newtag='git tag -a'

alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Play audio files in current dir by type
#alias playwav='deadbeef *.wav'
#alias playogg='deadbeef *.ogg'
#alias playmp3='deadbeef *.mp3'
#alias playm4a='deadbeef *.m4a'
#alias playflac='deadbeef *.flac'

alias playwav='mpv *.wav'
alias playogg='mpv *.ogg'
alias playmp3='mpv *.mp3'
alias playm4a='mpv *.m4a'
alias playflac='mpv *.flac'

# Play video files in current dir by type
alias playavi='mpv *.avi'
alias playmov='mpv *.mov'
alias playmp4='mpv *.mp4'
alias playmkv='mpv *.mkv'
alias playwebm='mpv *.webm'

# yt-dlp
alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-best="yt-dlp --extract-audio --audio-format best "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-m4a="yt-dlp --extract-audio --audio-format m4a "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias yta-opus="yt-dlp --extract-audio --audio-format opus "
alias yta-vorbis="yt-dlp --extract-audio --audio-format vorbis "
alias yta-wav="yt-dlp --extract-audio --audio-format wav "
alias ytv-best="yt-dlp -f bestvideo+bestaudio "

alias ytv-mine='yt-dlp -f "bv[height<=1080][vcodec^=avc]+ba[ext=m4a]" --embed-chapters --embed-metadata --embed-thumbnail --convert-thumbnail jpg --embed-subs --sub-langs en.*,all '
alias ytv-thb="yt-dlp --skip-download --write-thumbnail --convert-thumbnails png "

# ffmpeg
alias vid-res='ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 '

# yt-dlp [split options]

# tty-clock
alias tclk="tty-clock -C 4 -c -b -t -s"

# My custom neofetch
#neofetch --ascii Pictures/ASCII/xo --ascii_colors 2 5 6 1
#neofetch --kitty ~/Pictures/WallHaven/ --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/WallHaven/ --crop_mode fit --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/artStation/waifu1080.png --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/Imgur/cute.jpg --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#neofetch --ascii ~/Pictures/ascii/xoBraille --ascii_colors 1 2 3 4 5 6
#neofetch --kitty ~/Pictures/Snapseed/soulim/waifu/ --crop_mode fit --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/Snapseed/soulim/hero/ --crop_mode fit --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/.Infinity/MobileWallpaper-u5889s.jpg --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/.WallHaven-NSFW/wallhaven-ymr7mk.png --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#neofetch --kitty ~/Pictures/.Imgur-NSFW/nc450Dq.jpeg --crop_mode fill --xoffset 2 --yoffset 1 --size 23%
#source ~/.customNeofetch.sh

# Fetch Master 6000
# https://github.com/anhsirk0/fetch-master-6000

#eval "$(starship init bash)"

#inxi -I

#source ~/.config/myPrompt.sh

#exec fish
export PATH=/home/gideon/.cargo/bin:$PATH

#macchina

#pyfetch
