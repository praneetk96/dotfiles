# How to customize Firefox

Before you can do any CSS customization to firefox you have to first enable a preferences('flag' if you are familiar with chrome browser):
1. First of all, type `about:config` and hit enter in the addressbar, you might get a warning popup, just click on `Accept the Risk and Continue`.
2. in the `Search preference name` search bar, type `toolkit.legacyUserProfileCustomizations.stylesheets` and enable it.

head over to [FirefoxCSS-Store](https://firefoxcss-store.github.io/) and pick any css style of your choice, clone it into your chrome folder of firefox configuration. change directory to `~/.mozilla/firefox/`, there will be few folders but you have to head into folder which has `*.default-release` in the end of the folder's name, there will be only one folder by this extension and then into `chrome` folder.

`cd && cd .mozilla/firefox/RANDOM_CHARS.default-release/chrome`

Most probably that `chrome` folder will not be there if not there then create it, then just clone the CSS style of your choice, for example i really like the [minimal-functional-fox](https://github.com/mut-ex/minimal-functional-fox) so to use it.

```
cd .mozilla/firefox/RANDOM_CHARS.default-release/chrome
git clone https://github.com/mut-ex/minimal-functional-fox.git && cd minimal-functional-fox
```
copy all the files in the `chrome` folder. Done.
